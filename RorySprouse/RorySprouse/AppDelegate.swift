//
//  AppDelegate.swift
//  RorySprouse
//
//  Created by Bala Murugan on 12/22/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        if NSUserDefaults.standardUserDefaults().boolForKey("kAppFirstTimeLaunchFinished") == false {
            GTMOAuth2ViewControllerTouch.removeAuthFromKeychainForName(kKeychainItemName)
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "kAppFirstTimeLaunchFinished")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        
        self.checkAutologin()
        return true
    }
    
    func checkAutologin() {
        if window == nil {
            self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        }
        if kAppLoginAvailable == true && NSUserDefaults.standardUserDefaults().boolForKey(kAppLoggedInKey) == false {
            let storyBoard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
            let loginVC = storyBoard.instantiateViewControllerWithIdentifier("kLoginViewController") as! LoginViewController
            
            if window!.rootViewController != nil && !window!.rootViewController!.isKindOfClass(LoginViewController) {
                loginVC.view.frame = CGRectMake(-screenSize.width, 0, screenSize.width, screenSize.height)
                window?.addSubview(loginVC.view)
                UIView.animateWithDuration(0.3, animations: {
                    self.window!.rootViewController!.view.frame = CGRectMake(screenSize.width/3, 0, screenSize.width, screenSize.height)
                    loginVC.view.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
                    }, completion: { (finished) in
                        self.window?.rootViewController = loginVC
                })
            } else {
                self.window?.rootViewController = loginVC
                self.window?.makeKeyAndVisible()
            }
        } else {
            let storyBoard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
            let navigationVC = storyBoard.instantiateViewControllerWithIdentifier("kUINavigationViewController") as! UINavigationController
            
            if window!.rootViewController != nil && !window!.rootViewController!.isKindOfClass(UINavigationController) {
                navigationVC.view.frame = CGRectMake(screenSize.width, 0, screenSize.width, screenSize.height)
                window?.addSubview(navigationVC.view)
                UIView.animateWithDuration(0.3, animations: {
                    self.window!.rootViewController!.view.frame = CGRectMake(-screenSize.width/3, 0, screenSize.width, screenSize.height)
                    navigationVC.view.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
                    }, completion: { (finished) in
                        self.window?.rootViewController = navigationVC
                })
            } else {
                self.window?.rootViewController = navigationVC
                self.window?.makeKeyAndVisible()
            }
        }
    }
    
    @available(iOS 9.0, *)
    func application(application: UIApplication, openURL url: NSURL, options: [String: AnyObject]) -> Bool {
        return GIDSignIn.sharedInstance().handleURL(url, sourceApplication: options[UIApplicationOpenURLOptionsSourceApplicationKey] as? String, annotation: options[UIApplicationOpenURLOptionsAnnotationKey])
    }
    
    @available(iOS, introduced=8.0, deprecated=9.0)
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        return GIDSignIn.sharedInstance().handleURL(url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }
    
}

