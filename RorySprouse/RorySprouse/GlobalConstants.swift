//
//  Constants.swift
//  MConsentForms
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

public enum TextFormat : Int {
    case Default = 0
    case SocialSecurity
    case ToothNumber
    case Phone
    case ExtensionCode
    case Zipcode
    case Number
    case Date
    case Month
    case Year
    case DateInCurrentYear
    case DateIn1980
    case Time
    case MiddleInitial
    case State
    case Amount
    case Email
    case SecureText
    case AlphaNumeric
    case NumbersWithoutValidation

}

//KEYS
let kFormsCompletedNotification = "kAllFormsCompletedSuccessfully"
let kDateChangedNotification = "kDateChangedToNextDateNotificaion"

let kAppLoggedInKey = "kApploggedIn"
let kAppLoginUsernameKey = "kAppLoginUserName"
let kAppLoginPasswordKey = "kAppLoginPassword"

//VARIABLES
let kKeychainItemLoginName = "Rory Sprouse: Google Login"
let kKeychainItemName = "Rory Sprouse: Google Drive"
let kClientId = "525832274595-tmnm8ehdcdj4lhiaqb64pn94a2a04rf8.apps.googleusercontent.com"
let kClientSecret = "Y3P9a8GPUuDfH6TVhB2X5iAg"
let kFolderName = "RorySprouse"

let kDentistNames: [String] = ["DR. RORY B. SPROUSE"]
let kDentistNameNeededForms = [kNewPatientSignInForm]
let kClinicName = "RORY B. SPROUSE"
let kAppName = "RORY B. SPROUSE"
let kPlace = "MARIETTA, GA"
let kState = "GA"
let kAppKey = "mcBlackWarrior"
let kAppLoginAvailable: Bool = false
let kCommonDateFormat = "MMM dd, yyyy"

let kGoogleID = "demo@srswebsolutions.com"
let kGooglePassword = "Srsweb123#"
let kOutputFileMustBeImage: Bool = false
//END OF VARIABLES

//FORMS
let kNewPatientSignInForm = "NEW PATIENT SIGN-IN FORM"
let kMedicalHistory = "MEDICAL HISTORY FORM"

//CONSENT FORMS
let kConsentForms = "CONSENT FORMS"

let kInsuranceCard = "SCAN INSURANCE CARD"
let kDrivingLicense = "SCAN DRIVING LICENSE"

//let kFeedBack = "CUSTOMER REVIEW FORM"
//let kVisitorCheckForm = "VISITOR CHECK IN FORM"
// END OF FORMS

let toothNumberRequired: [String] = [""]
//Replace '""' with form names
let consentIndex: Int = 4


