//
//  HippaSignatureRefusal.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/12/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HipaaSignatureRefusalPopup: UIView {

    @IBOutlet weak var radioRefusal: RadioButton!
    @IBOutlet weak var textFieldRefusalOther: MCTextField!
    
    class func popUpView() -> HipaaSignatureRefusalPopup {
        return NSBundle.mainBundle().loadNibNamed("HipaaSignatureRefusalPopup", owner: nil, options: nil)!.first as! HipaaSignatureRefusalPopup
    }
    
    var completion:((HipaaSignatureRefusalPopup, Bool, UITextField, Int)->Void)?
    var errorBlock:((String) -> Void)?
    
    func showInViewController(viewController: UIViewController?, completion : (popUpView: HipaaSignatureRefusalPopup, refused: Bool, textFieldOther : UITextField, refusalTag: Int) -> Void, error: (message: String) -> Void) {
        
        textFieldRefusalOther.textColor = UIColor.lightGrayColor()
        textFieldRefusalOther.enabled = false
        
        self.completion = completion
        self.errorBlock = error
        self.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        
        if viewController != nil {
            viewController!.view.addSubview(self)
        } else {
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.window!.addSubview(self)
        }
        self.subviews[0].transform = CGAffineTransformMakeScale(0.1, 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonActionOK() {
        if radioRefusal.selectedButton != nil && radioRefusal.selectedButton.tag == 4 && textFieldRefusalOther.isEmpty {
            self.errorBlock?("PLEASE ENTER THE REASON")
        } else {
            self.completion?(self, radioRefusal.selectedButton != nil, self.textFieldRefusalOther, radioRefusal.selectedButton == nil ? 0 : radioRefusal.selectedButton.tag)
        }
    }
    
    @IBAction func buttonRefusalAction() {
        if radioRefusal.selectedButton.tag == 4 {
            textFieldRefusalOther.enabled = true
            textFieldRefusalOther.textColor = UIColor.blackColor()
        } else {
            textFieldRefusalOther.text = ""
            textFieldRefusalOther.enabled = false
            textFieldRefusalOther.textColor = UIColor.lightGrayColor()
        }
    }
    
    @IBAction func buttonSkipAction () {
        completion?(self, false, self.textFieldRefusalOther, 0)
    }
    
    func close() {
        self.removeFromSuperview()
    }
}
