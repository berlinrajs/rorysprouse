//
//  LoginViewController.swift
//  OptimaDentistry
//
//  Created by SRS Web Solutions on 12/07/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class LoginViewController: MCViewController {

    @IBOutlet weak var textFieldUserName: MCTextField!
    @IBOutlet weak var textFieldPassword: MCTextField!
    @IBOutlet weak var labelVersion: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelPlace: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textFieldUserName.textFormat = .Email
        textFieldPassword.textFormat = .SecureText
        labelPlace.text = kPlace
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(dateChangedNotification), name: kDateChangedNotification, object: nil)
        self.dateChangedNotification()
        labelVersion.text = NSBundle.mainBundle().objectForInfoDictionaryKey(kCFBundleVersionKey as String) as? String
        // Do any additional setup after loading the view.
    }
    
    func dateChangedNotification() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = kCommonDateFormat
        labelDate.text = dateFormatter.stringFromDate(NSDate()).uppercaseString
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonSubmitAction() {
        if textFieldUserName.isEmpty || !textFieldUserName.text!.isValidEmail {
            self.showAlert("PLEASE ENTER THE VALID EMAIL")
        } else if textFieldPassword.isEmpty {
            self.showAlert("PLEASE ENTER THE PASSWORD")
        } else {
            self.submitAction()
        }
    }
    
    func submitAction() {
        self.view.endEditing(true)
        if Reachability.isConnectedToNetwork() {
            ServiceManager.loginWithUsername(textFieldUserName.text!, password: textFieldPassword.text!) { (success, error) -> (Void) in
                if success {
                    NSUserDefaults.standardUserDefaults().setBool(true, forKey: kAppLoggedInKey)
                    NSUserDefaults.standardUserDefaults().setValue(self.textFieldUserName.text!, forKey: kAppLoginUsernameKey)
                    NSUserDefaults.standardUserDefaults().setValue(self.textFieldPassword.text!, forKey: kAppLoginPasswordKey)
                    NSUserDefaults.standardUserDefaults().synchronize()
                    (UIApplication.sharedApplication().delegate as! AppDelegate).checkAutologin()
                } else {
                    if error == nil {
                        self.showAlert("PLEASE CHECK YOUR INTERNET CONNECTION AND TRY AGAIN")
                    } else {
                        self.showAlert(error!.localizedDescription.uppercaseString)
                    }
                }
            }
        } else {
            self.showAlert("Your device is not connected to internet. Please go to settings to connect.", buttonTitles: ["Settings", "Cancel"], completion: { (buttonIndex) in
                if buttonIndex == 0 {
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.sharedApplication().openURL(url)
                    }
                }
            })
        }
    }
}
extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == textFieldUserName {
            textFieldPassword.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            self.submitAction()
        }
        return true
    }
}
