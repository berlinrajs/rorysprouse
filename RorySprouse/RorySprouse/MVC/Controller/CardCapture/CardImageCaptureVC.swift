//
//  CardImageCaptureVC.swift
//  AceDental
//
//  Created by SRS Web Solutions on 29/04/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class CardImageCaptureVC: MCViewController {

    @IBOutlet weak var imageViewFront: ActionImageView!
    @IBOutlet weak var imageViewBack: ActionImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
    var isDrivingLicense: Bool = false
    var isFrontImageSelected: Bool = false
    var isBackImageSelected: Bool = false
    
    var frontPlaceHolderImage: UIImage? {
        get {
            return isDrivingLicense == true ? UIImage(named: "LicenseFront") : UIImage(named: "InsuranceFront")
        }
    }
    
    var backPlaceHolderImage: UIImage? {
        get {
            return isDrivingLicense == true ? UIImage(named: "LicenseBack") : UIImage(named: "InsuranceBack")
        }
    }
    
    var selectedButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelTitle.text = isDrivingLicense ? "ADD/UPDATE DRIVING LICENSE" : "ADD/UPDATE INSURANCE CARD"
      
        imageViewFront.cornerRadius = 5.0
        imageViewBack.cornerRadius = 5.0
        imageViewBack.borderColor = UIColor.clearColor()
        imageViewFront.borderColor = UIColor.clearColor()
        
        imageViewFront.image = self.frontPlaceHolderImage
        imageViewBack.image = self.backPlaceHolderImage
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    
    @IBAction func nextAction(sender: AnyObject) {
        
        if isDrivingLicense && !isFrontImageSelected {
            self.showAlert("PLEASE CAPTURE FRONT SIDE OF THE CARD")
            
        } else if !isDrivingLicense && !isFrontImageSelected  {
            self.showAlert("PLEASE CAPTURE FRONT SIDE OF THE CARD")
            
        } else {
            let formVC = self.storyboard?.instantiateViewControllerWithIdentifier("kCardImageFormVC") as! CardImageFormVC
            formVC.patient = self.patient
            formVC.isDrivingLicense = self.isDrivingLicense
            formVC.frontImage = self.imageViewFront.image!
            formVC.backImage = isBackImageSelected ? self.imageViewBack.image : nil
            navigationController?.pushViewController(formVC, animated: true)
        }
    }

    @IBAction func camButtonSelected(sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.Camera) {
            let picker = self.storyboard?.instantiateViewControllerWithIdentifier("kCardImagePickerControllerNew") as! CardImagePickerControllerNew
            picker.delegate = self
            self.presentViewController(picker, animated: true) {
                self.selectedButton = sender
            }
        }
    }
}
extension CardImageCaptureVC: CardImageCaptureDelegate {
    func cardImagePicker(picker: CardImagePickerControllerNew, completedWithCardImage image: UIImage?) {
        if image != nil {
            if selectedButton.tag == 1 {
                imageViewFront.image = image
                isFrontImageSelected = true
                selectedButton.selected = true
                
                imageViewFront.layer.borderColor = UIColor.whiteColor().CGColor
            } else {
                imageViewBack.image = image
                isBackImageSelected = true
                selectedButton.selected = true
                
                imageViewBack.layer.borderColor = UIColor.whiteColor().CGColor
            }
        }
        picker.dismissViewControllerAnimated(true) {
            self.selectedButton = nil
        }
    }
    func cardImagePickerDidCancel(picker: CardImagePickerControllerNew){
        picker.dismissViewControllerAnimated(true) {
            self.selectedButton = nil
        }
    }
}
