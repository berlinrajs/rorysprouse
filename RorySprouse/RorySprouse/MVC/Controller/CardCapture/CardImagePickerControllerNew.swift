//
//  CardImagePickerControllerNew.swift
//  ABCES
//
//  Created by Berlin Raj on 10/10/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
import AVFoundation

protocol CardImageCaptureDelegate {
    func cardImagePicker(picker: CardImagePickerControllerNew, completedWithCardImage image: UIImage?)
    func cardImagePickerDidCancel(picker: CardImagePickerControllerNew)
}

class CardImagePickerControllerNew: UIViewController {
    
    var captureSession = AVCaptureSession()
    var stillImageOutput: AVCaptureStillImageOutput!
    
    @IBOutlet var previewView: UIView!
    var delegate: CardImageCaptureDelegate!
    
    var isFrontCameraActive: Bool = false
    var capturePressed: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        edgesForExtendedLayout = .None
        captureSession.beginConfiguration()
        captureSession.sessionPreset = AVCaptureSessionPreset640x480
        
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = CGRectMake(130, 292, 505, 326)
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.previewView.layer.addSublayer(previewLayer)
        
        let device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        isFrontCameraActive = device.position == AVCaptureDevicePosition.Front ? true : false
        do {
            let input = try AVCaptureDeviceInput(device: device)
            captureSession.addInput(input)
            
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            captureSession.addOutput(stillImageOutput)
        } catch {
            
        }
        
        captureSession.commitConfiguration()
        captureSession.startRunning()
    }
    @IBAction func toogleDevice() {
        if capturePressed == true {
            capturePressed = false
            return
        }
        captureSession.stopRunning()
        captureSession.beginConfiguration()
        
        for input in captureSession.inputs {
            captureSession.removeInput(input as! AVCaptureInput)
        }
        
        for device in AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo) {
            if (device.position == AVCaptureDevicePosition.Front && isFrontCameraActive == false) || (device.position == AVCaptureDevicePosition.Back && isFrontCameraActive == true) {
                do {
                    let input = try AVCaptureDeviceInput(device: device as! AVCaptureDevice)
                    captureSession.addInput(input)
                } catch {
                    
                }
            }
        }
        self.isFrontCameraActive = !isFrontCameraActive
        captureSession.commitConfiguration()
        captureSession.startRunning()
    }
    
    @IBAction func captureImage() {
        capturePressed = true
        var videoConnection: AVCaptureConnection!
        
        for connection in stillImageOutput.connections {
            for port in connection.inputPorts {
                if port.mediaType == AVMediaTypeVideo {
                    videoConnection = connection as! AVCaptureConnection
                    break;
                }
            }
            if videoConnection != nil
            {
                break;
            }
        }
        print("about to request a capture from: \(stillImageOutput)")
        stillImageOutput.captureStillImageAsynchronouslyFromConnection(videoConnection) { (buffer, error) in
            let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(buffer)
            let image = UIImage(data: imageData)
            
            self.delegate.cardImagePicker(self, completedWithCardImage: image)
            self.capturePressed = false
        }
    }
    
    @IBAction func backAction() {
        if capturePressed == true {
            capturePressed = false
            return
        }
        captureSession.stopRunning()
        delegate.cardImagePickerDidCancel(self)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
