//
//  NewPatient1ViewController.swift
//  RorySprouse
//
//  Created by Bala Murugan on 12/22/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient1ViewController: MCViewController {

    @IBOutlet weak var textfieldAddress : MCTextField!
    @IBOutlet weak var textfieldCity : MCTextField!
    @IBOutlet weak var textfieldState : MCTextField!
    @IBOutlet weak var textfieldZipcode : MCTextField!
    @IBOutlet weak var textfieldSSN : MCTextField!
    @IBOutlet weak var dropDownGender : BRDropDown!
    @IBOutlet weak var dropDownMaritialStatus : BRDropDown!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldState.textFormat = .State
        textfieldZipcode.textFormat = .Zipcode
        textfieldSSN.textFormat = .SocialSecurity
        dropDownGender.items = ["MALE", "FEMALE"]
        dropDownGender.placeholder = "-- SELECT GENDER * --"
        dropDownMaritialStatus.items = ["MARRIED","SINGLE","MINOR"]
        dropDownMaritialStatus.placeholder = "-- SELECT MARITIAL STATUS * --"
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        textfieldAddress.setSavedText(patient.address)
        textfieldCity.setSavedText(patient.city)
        textfieldState.setSavedText(patient.state)
        textfieldZipcode.setSavedText(patient.zipcode)
        textfieldSSN.setSavedText(patient.socialSecurityNumber)
        dropDownGender.selectedIndex = patient.genderTag
        dropDownMaritialStatus.selectedIndex = patient.maritialStatusTag
        
    }
    
    func saveValue (){
        patient.address = textfieldAddress.getText()
        patient.city = textfieldCity.getText()
        patient.state = textfieldState.getText()
        patient.zipcode = textfieldZipcode.getText()
        patient.socialSecurityNumber = textfieldSSN.getText()
        patient.genderTag = dropDownGender.selectedOption == nil ? 0 : dropDownGender.selectedIndex
        patient.maritialStatusTag = dropDownMaritialStatus.selectedOption == nil ? 0 : dropDownMaritialStatus.selectedIndex
        
    }

    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }

    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textfieldAddress.isEmpty || textfieldCity.isEmpty || textfieldState.isEmpty || textfieldZipcode.isEmpty{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if !textfieldZipcode.text!.isZipCode{
            self.showAlert("PLEASE ENTER THE VALID ZIPCODE")
        }else if !textfieldSSN.isEmpty && !textfieldSSN.text!.isSocialSecurityNumber{
            self.showAlert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")
        }else if dropDownGender.selectedOption == nil{
            self.showAlert("PLEASE SELECT THE GENDER")
        }else if dropDownMaritialStatus.selectedOption == nil{
            self.showAlert("PLEASE SELECT THE MARITIAL STATUS")
        }else{
            saveValue()
            let newPatient = newPatientStoryboard.instantiateViewControllerWithIdentifier("NewPatient2VC") as! NewPatient2ViewController
            newPatient.patient = self.patient
            self.navigationController?.pushViewController(newPatient, animated: true)

        }
    }

}
