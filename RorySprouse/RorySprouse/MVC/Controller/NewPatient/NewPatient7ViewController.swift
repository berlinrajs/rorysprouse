//
//  NewPatient7ViewController.swift
//  RorySprouse
//
//  Created by Bala Murugan on 12/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient7ViewController: MCViewController {
    
    @IBOutlet weak var textviewReason : MCTextView!
    @IBOutlet weak var textfieldLastDentalCleaning : MCTextField!
    @IBOutlet weak var textviewConcerns : MCTextView!
    @IBOutlet weak var dropdownFloss : BRDropDown!
    @IBOutlet weak var dropdownBrush : BRDropDown!
    @IBOutlet weak var textfieldCleanMouth : MCTextField!
    @IBOutlet weak var radioTeethLifetime : RadioButton!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldLastDentalCleaning.textFormat = .DateInCurrentYear
        dropdownBrush.items = ["ONCE A DAY","TWICE A DAY","THRICE A DAY","NEVER"]
        dropdownBrush.placeholder = "-- SELECT --"
        dropdownFloss.items = ["ONCE A WEEK","TWICE A WEEK","DAILY","NEVER"]
        dropdownFloss.placeholder = "-- SELECT --"
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        textviewReason.placeholder = "PLEASE TYPE HERE"
        textviewReason.placeholderColor = UIColor.lightGrayColor()
        textviewReason.text = patient.reasonTodayVisit == "N/A" ? "PLEASE TYPE HERE" : patient.reasonTodayVisit
        textviewReason.textColor = patient.reasonTodayVisit == "N/A" ? UIColor.lightGrayColor() : UIColor.blackColor()
        
        textviewConcerns.placeholder = "PLEASE TYPE HERE"
        textviewConcerns.placeholderColor = UIColor.lightGrayColor()
        textviewConcerns.text = patient.specificConcerns == "N/A" ? "PLEASE TYPE HERE" : patient.specificConcerns
        textviewConcerns.textColor = patient.specificConcerns == "N/A" ? UIColor.lightGrayColor() : UIColor.blackColor()
        
        textfieldLastDentalCleaning.setSavedText(patient.lastDentalCleaning)
        textfieldCleanMouth.setSavedText(patient.cleanMouth)
        radioTeethLifetime.setSelectedWithTag(patient.radioTeethLifetimeTag)
        dropdownBrush.selectedIndex = patient.brushTag
        dropdownFloss.selectedIndex = patient.flossTag
    }
    
    func saveValue (){
        patient.reasonTodayVisit = textviewReason.isEmpty ? "N/A" : textviewReason.text!
        patient.specificConcerns = textviewConcerns.isEmpty ? "N/A" : textviewConcerns.text!
        patient.lastDentalCleaning = textfieldLastDentalCleaning.getText()
        patient.flossTag = dropdownFloss.selectedOption == nil ? 0 : dropdownFloss.selectedIndex
        patient.floss = dropdownFloss.selectedOption == nil ? "N/A" : dropdownFloss.selectedOption!
        patient.brushTag = dropdownBrush.selectedOption == nil ? 0 : dropdownBrush.selectedIndex
        patient.brush = dropdownBrush.selectedOption == nil ? "N/A" : dropdownBrush.selectedOption!
        patient.cleanMouth = textfieldCleanMouth.getText()
        patient.radioTeethLifetimeTag = radioTeethLifetime.selectedButton == nil ? 0 : radioTeethLifetime.selectedButton.tag



        
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }


    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textviewReason.isEmpty || dropdownFloss.selectedOption == nil || dropdownBrush.selectedOption == nil || radioTeethLifetime.selectedButton == nil{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else{
            saveValue()
            let newPatient = newPatientStoryboard.instantiateViewControllerWithIdentifier("NewPatient8VC") as! NewPatient8ViewController
            newPatient.patient = self.patient
            self.navigationController?.pushViewController(newPatient, animated: true)

        }
    }
    
}
