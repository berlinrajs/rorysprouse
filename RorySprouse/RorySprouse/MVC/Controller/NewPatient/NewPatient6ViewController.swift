//
//  NewPatient6ViewController.swift
//  RorySprouse
//
//  Created by Bala Murugan on 12/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient6ViewController: MCViewController {

    @IBOutlet weak var textfieldRelation : MCTextField!
    @IBOutlet weak var textfieldName : MCTextField!
    @IBOutlet weak var textfieldAddress : MCTextField!
    @IBOutlet weak var textfieldCity : MCTextField!
    @IBOutlet weak var textfieldState : MCTextField!
    @IBOutlet weak var textfieldZip : MCTextField!
//    @IBOutlet weak var textfieldPhone : MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textfieldState.textFormat = .State
        textfieldZip.textFormat = .Zipcode
//        textfieldPhone.textFormat = .Phone
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        textfieldRelation.setSavedText(patient.responsibleRelation)
        textfieldName.setSavedText(patient.responsibleName)
        textfieldAddress.setSavedText(patient.responsibleAddress)
        textfieldCity.setSavedText(patient.responsibleCity)
        textfieldState.setSavedText(patient.responsibleState)
        textfieldZip.setSavedText(patient.responsibleZipcode)
//        textfieldPhone.setSavedText(patient.responsiblePhone)

    }
    
    func saveValue (){
        patient.responsibleRelation = textfieldRelation.getText()
        patient.responsibleName = textfieldName.getText()
        patient.responsibleAddress = textfieldAddress.getText()
        patient.responsibleCity = textfieldCity.getText()
        patient.responsibleState = textfieldState.getText()
        patient.responsibleZipcode = textfieldZip.getText()
//        patient.responsiblePhone = textfieldPhone.getText()

        
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
//        if !textfieldPhone.isEmpty && !textfieldPhone.text!.isPhoneNumber{
//            self.showAlert("PLEASE ENTER THE VALID PHONE NUMBER")
//        }else 
        if !textfieldZip.isEmpty && !textfieldZip.text!.isZipCode{
            self.showAlert("PLEASE ENTER THE VALID ZIPCODE")
        }else{
            saveValue()
            let newPatient = newPatientStoryboard.instantiateViewControllerWithIdentifier("ResponsiblePartyVC") as! ResponsiblePartyViewController
            newPatient.patient = self.patient
            self.navigationController?.pushViewController(newPatient, animated: true)
        }
    }
    



}
