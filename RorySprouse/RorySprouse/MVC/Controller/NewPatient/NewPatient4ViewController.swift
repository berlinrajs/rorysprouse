//
//  NewPatient4ViewController.swift
//  RorySprouse
//
//  Created by Bala Murugan on 12/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient4ViewController: MCViewController {

    @IBOutlet weak var textfieldLegalName : MCTextField!
    @IBOutlet weak var textfieldBirthdate : MCTextField!
    @IBOutlet weak var textfieldSSN : MCTextField!
    @IBOutlet weak var textfieldInsuranceCompany : MCTextField!
    @IBOutlet weak var textfieldGroup : MCTextField!
    @IBOutlet weak var textfieldAddress : MCTextField!
    @IBOutlet weak var textfieldCity : MCTextField!
    @IBOutlet weak var textfieldState : MCTextField!
    @IBOutlet weak var textfieldZipcode : MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldGroup.textFormat = .Number
        textfieldBirthdate.textFormat = .DateIn1980
        textfieldSSN.textFormat = .SocialSecurity
        textfieldState.textFormat = .State
        textfieldZipcode.textFormat = .Zipcode
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        let insurance : Insurance = patient.primaryInsurance
        textfieldLegalName.setSavedText(insurance.LegalName)
        textfieldBirthdate.setSavedText(insurance.birthdate)
        textfieldSSN.setSavedText(insurance.socialSecurity)
        textfieldInsuranceCompany.setSavedText(insurance.insuranceCompany)
        textfieldGroup.setSavedText(insurance.group)
        textfieldAddress.setSavedText(insurance.address)
        textfieldCity.setSavedText(insurance.city)
        textfieldState.text = insurance.state == "N/A" ? "GA" : insurance.state
        textfieldZipcode.setSavedText(insurance.zipcode)


        
    }
    
    func saveValue (){
        let insurance : Insurance = patient.primaryInsurance
        insurance.LegalName = textfieldLegalName.getText()
        insurance.birthdate = textfieldBirthdate.getText()
        insurance.socialSecurity = textfieldSSN.getText()
        insurance.insuranceCompany = textfieldInsuranceCompany.getText()
        insurance.group = textfieldGroup.getText()
        insurance.address = textfieldAddress.getText()
        insurance.city = textfieldCity.getText()
        insurance.state = textfieldState.getText()
        insurance.zipcode = textfieldZipcode.getText()

        insurance.group = textfieldGroup.getText()

        
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldSSN.isEmpty && !textfieldSSN.text!.isSocialSecurityNumber{
            self.showAlert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")
        }else if !textfieldZipcode.isEmpty && !textfieldZipcode.text!.isZipCode{
            self.showAlert("PLEASE ENTER THE VALID ZIPCODE")
        }else{
            saveValue()
            let newPatient = newPatientStoryboard.instantiateViewControllerWithIdentifier("NewPatient5VC") as! NewPatient5ViewController
            newPatient.patient = self.patient
            self.navigationController?.pushViewController(newPatient, animated: true)
        }
        
    }

}

