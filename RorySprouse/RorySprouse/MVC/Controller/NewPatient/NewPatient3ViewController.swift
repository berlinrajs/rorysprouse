//
//  NewPatient3ViewController.swift
//  RorySprouse
//
//  Created by Bala Murugan on 12/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient3ViewController: MCViewController {

    @IBOutlet weak var textfieldName : MCTextField!
    @IBOutlet weak var textfieldPhone : MCTextField!
    @IBOutlet weak var textfieldReference : MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textfieldPhone.textFormat = .Phone
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        textfieldName.setSavedText(patient.emergencyContactName)
        textfieldPhone.setSavedText(patient.emergencyContactPhone)
        textfieldReference.setSavedText(patient.reference)

        
    }
    
    func saveValue (){
        patient.emergencyContactName = textfieldName.getText()
        patient.emergencyContactPhone = textfieldPhone.getText()
        patient.reference = textfieldReference.getText()
        
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldPhone.isEmpty && !textfieldPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID PHONE NUMBER")
        }else{
            saveValue()
            let newPatient = newPatientStoryboard.instantiateViewControllerWithIdentifier("NewPatient6VC") as! NewPatient6ViewController
            newPatient.patient = self.patient
            self.navigationController?.pushViewController(newPatient, animated: true)

//            YesOrNoAlert.sharedInstance.showWithTitle("DO YOU HAVE DENTAL INSURANCE", button1Title: "YES", button2Title: "NO", completion: { (buttonIndex) in
//                if buttonIndex == 1{
//                    self.patient.haveInsurance = true
//                    let newPatient = newPatientStoryboard.instantiateViewControllerWithIdentifier("NewPatient4VC") as! NewPatient4ViewController
//                    newPatient.patient = self.patient
//                    newPatient.isPrimary = true
//                    self.navigationController?.pushViewController(newPatient, animated: true)
//
//                }else{
//                    self.patient.haveInsurance = false
//                    self.patient.primaryInsurance = Insurance()
//                    self.patient.secondaryInsurance = Insurance()
//                    if !self.patient.is18YearsOld{
//                        let newPatient = newPatientStoryboard.instantiateViewControllerWithIdentifier("NewPatient6VC") as! NewPatient6ViewController
//                        newPatient.patient = self.patient
//                        self.navigationController?.pushViewController(newPatient, animated: true)
//                    }else{
//                        let newPatient = newPatientStoryboard.instantiateViewControllerWithIdentifier("NewPatient7VC") as! NewPatient7ViewController
//                        newPatient.patient = self.patient
//                        self.navigationController?.pushViewController(newPatient, animated: true)
//                    }
//                }
//            })
            
        }
    }
    

}
