//
//  PatientSignInStep4VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class NewPatient10ViewController: MCViewController {

    @IBOutlet weak var textViewListMedications: MCTextView!
    @IBOutlet weak var radioWomanPreg: RadioButton!
    @IBOutlet weak var womanView: UIView!
    @IBOutlet weak var textfieldPhysicianName : MCTextField!
    @IBOutlet weak var textfieldLastVisit : MCTextField!
    @IBOutlet weak var radioSeriousIllness : RadioButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textViewListMedications.placeholder = "PLEASE TYPE HERE"
        textViewListMedications.placeholderColor = UIColor.lightGrayColor()
        textfieldLastVisit.textFormat = .DateInCurrentYear
        
        if patient.genderTag == 2{
            womanView.alpha = 1.0
            womanView.userInteractionEnabled = true
        }
        loadValues()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction override func buttonBackAction() {
        saveValues()
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func onSeriousIllnessButtonPressed (withSender sender : RadioButton){
        if sender.tag == 1{
            PopupTextView.popUpView().showWithPlaceHolder("PLEASE TYPE HERE", completion: { (popUpView, textView) in
                if textView.isEmpty{
                    sender.setSelectedWithTag(2)
                    self.patient.seriousIllness = "N/A"
                }else{
                    self.patient.seriousIllness = textView.text!

                }
                popUpView.close()
            })
        }else{
            self.patient.seriousIllness = "N/A"
        }
    }
    
    func saveValues(){
        patient.womanRuPregnant = radioWomanPreg.selectedButton == nil ? 0 : radioWomanPreg.selectedButton.tag
        patient.physicianName = textfieldPhysicianName.getText()
        patient.physicianLastVisit = textfieldLastVisit.getText()
        patient.seriousIllnessTag = radioSeriousIllness.selectedButton.tag
        patient.listMedications = textViewListMedications.isEmpty ? "N/A" : textViewListMedications.text!
    }
    
    func loadValues(){
        if patient.genderTag == 2{
        radioWomanPreg.setSelectedWithTag(patient.womanRuPregnant)
        }
        textfieldPhysicianName.setSavedText(patient.physicianName)
        textfieldLastVisit.setSavedText(patient.physicianLastVisit)
        radioSeriousIllness.setSelectedWithTag(patient.seriousIllnessTag)
        textViewListMedications.text = patient.listMedications == "N/A" ? "PLEASE TYPE HERE" : patient.listMedications
        textViewListMedications.textColor = patient.listMedications == "N/A" ? UIColor.lightGrayColor() : UIColor.blackColor()
    }

    
    
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
            saveValues()
        let newPatient = newPatientStoryboard.instantiateViewControllerWithIdentifier("NewPatient12VC") as! NewPatient12ViewController
        newPatient.patient = self.patient
        self.navigationController?.pushViewController(newPatient, animated: true)
        
    }
}
