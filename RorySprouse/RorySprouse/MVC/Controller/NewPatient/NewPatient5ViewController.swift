//
//  NewPatient5ViewController.swift
//  RorySprouse
//
//  Created by Bala Murugan on 12/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient5ViewController: MCViewController {
    
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    //    @IBOutlet weak var labelPatientName : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        labelPatientName.text = patient.fullName
        labelDate.todayDate = patient.dateToday
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            patient.insuranceSignature = signaturePatient.signatureImage()
            let newPatient = newPatientStoryboard.instantiateViewControllerWithIdentifier("NewPatient7VC") as! NewPatient7ViewController
            newPatient.patient = self.patient
            self.navigationController?.pushViewController(newPatient, animated: true)
            
        }
    }
    
    
    
}
