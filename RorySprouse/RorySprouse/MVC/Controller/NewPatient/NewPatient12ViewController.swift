//
//  NewPatient5ViewController.swift
//  RorySprouse
//
//  Created by Bala Murugan on 12/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient12ViewController: MCViewController {
    
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var signatureLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        signatureLabel.text = "Patient Signature"
        labelPatientName.text = patient.fullName
        labelDate.todayDate = patient.dateToday
    }
    
    func saveValues() {
        patient.financialSignature = signaturePatient.signatureImage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        } else {
            
            saveValues()
            if patient.signRefused == true{
                let newPatient = newPatientStoryboard.instantiateViewControllerWithIdentifier("kNewPatientFormViewController") as! NewPatientFormViewController
                newPatient.patient = self.patient
                self.navigationController?.pushViewController(newPatient, animated: true)
            }else{
                let newPatient = newPatientStoryboard.instantiateViewControllerWithIdentifier("PrivacyPractice1ViewController") as! PrivacyPractice1ViewController
                newPatient.patient = self.patient
                self.navigationController?.pushViewController(newPatient, animated: true)
            }
            
            
            
        }
    }
}
