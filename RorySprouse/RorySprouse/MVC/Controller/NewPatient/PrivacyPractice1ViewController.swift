//
//  PrivacyPractice1ViewController.swift
//  PeopleCenterDental
//
//  Created by Bala Murugan on 11/14/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class PrivacyPractice1ViewController: MCViewController {

    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        labelDate.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            patient.privacyAcknSignature = signaturePatient.signatureImage()
            let newPatient = newPatientStoryboard.instantiateViewControllerWithIdentifier("kNewPatientFormViewController") as! NewPatientFormViewController
            newPatient.patient = self.patient
            self.navigationController?.pushViewController(newPatient, animated: true)
        }
    }
    
}
