//
//  NewPatient2ViewController.swift
//  RorySprouse
//
//  Created by Bala Murugan on 12/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient2ViewController: MCViewController {

//    @IBOutlet weak var radioReminderOptions : RadioButton!
    @IBOutlet weak var textfieldHomePhone : MCTextField!
    @IBOutlet weak var textfieldWorkPhone : MCTextField!
    @IBOutlet weak var textfieldMobilePhone : MCTextField!
    @IBOutlet weak var textfieldEmail : MCTextField!
    @IBOutlet weak var textfieldEmployer : MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldHomePhone.textFormat = .Phone
        textfieldWorkPhone.textFormat = .Phone
        textfieldMobilePhone.textFormat = .Phone
        textfieldEmail.textFormat = .Email
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        textfieldHomePhone.setSavedText(patient.homephone)
        textfieldWorkPhone.setSavedText(patient.workPhone)
        textfieldMobilePhone.setSavedText(patient.mobilePhone)
        textfieldEmail.setSavedText(patient.email)
        textfieldEmployer.setSavedText(patient.employerName)
//        radioReminderOptions.setSelectedWithTag(patient.reminderOptionTag)
        
    }
    
    func saveValue (){
        patient.homephone = textfieldHomePhone.getText()
        patient.workPhone = textfieldWorkPhone.getText()
        patient.mobilePhone = textfieldMobilePhone.getText()
        patient.email = textfieldEmail.getText()
        patient.employerName = textfieldEmployer.getText()
//        patient.reminderOptionTag = radioReminderOptions.selectedButton == nil ? 0 : radioReminderOptions.selectedButton.tag
        
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldHomePhone.isEmpty && !textfieldHomePhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID HOME PHONE NUMBER")
        }else if !textfieldWorkPhone.isEmpty && !textfieldWorkPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID WORK PHONE NUMBER")
        }else if !textfieldMobilePhone.isEmpty && !textfieldMobilePhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID CELL PHONE NUMBER")
        }else if !textfieldEmail.isEmpty && !textfieldEmail.text!.isValidEmail{
            self.showAlert("PLEASE ENTER THE VALID EMAIL ADDRESS")
        }else{
            saveValue()
            let newPatient = newPatientStoryboard.instantiateViewControllerWithIdentifier("NewPatient3VC") as! NewPatient3ViewController
            newPatient.patient = self.patient
            self.navigationController?.pushViewController(newPatient, animated: true)
            
        }
    }
    


}
