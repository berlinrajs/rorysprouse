//
//  NewPatient9ViewController.swift
//  RorySprouse
//
//  Created by Bala Murugan on 12/27/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient9ViewController: MCViewController {

    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var buttonVerified: UIButton!
    @IBOutlet var buttonNext: UIButton!
    @IBOutlet var buttonBack1: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onBackButton1Pressed (withSender sender: AnyObject) {
            self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func buttonVerifiedAction(withSender sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if buttonVerified.selected == false {
            self.showAlert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
        } else {
                let newPatient = newPatientStoryboard.instantiateViewControllerWithIdentifier("NewPatient10VC") as! NewPatient10ViewController
                newPatient.patient = self.patient
                self.navigationController?.pushViewController(newPatient, animated: true)
            }
        }

}
extension NewPatient9ViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.medicalHistory.allergies.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return  44
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! PatientInfoCell
        cell.delegate = self
        cell.tag = indexPath.row
        cell.configCell(self.patient.medicalHistory.allergies[indexPath.row])
        
        return cell
    }
}

extension NewPatient9ViewController : PatientInfoCellDelegate{
    
    func radioButtonTappedForCell(cell: PatientInfoCell) {
        PopupTextField.popUpView().showInViewController(self, WithTitle: "", placeHolder: "PLEASE SPECIFY", textFormat: TextFormat.Default) { (popUpView, textField) in
            
            if textField.isEmpty{
                cell.radioButtonYes.selected = false
                self.patient.medicalHistory.allergies[cell.tag].selectedOption = false
                
            }else{
                self.patient.medicalHistory.allergies[cell.tag].answer = textField.text
                
            }
            popUpView.close()
        }
    }
}

