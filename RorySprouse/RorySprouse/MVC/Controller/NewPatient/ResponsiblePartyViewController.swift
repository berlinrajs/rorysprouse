//
//  ResponsiblePartyViewController.swift
//  RorySprouse
//
//  Created by Bala Murugan on 1/5/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class ResponsiblePartyViewController: MCViewController {
    
    @IBOutlet weak var textfieldBirthdate : MCTextField!
    @IBOutlet weak var textfieldSSN : MCTextField!
    @IBOutlet weak var textfieldEmployer : MCTextField!
    @IBOutlet weak var textfieldWorkPhone : MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldBirthdate.textFormat = .DateIn1980
        textfieldSSN.textFormat = .SocialSecurity
        textfieldWorkPhone.textFormat = .Phone
        loadValue()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        textfieldBirthdate.setSavedText(patient.responsibleBirthdate)
        textfieldSSN.setSavedText(patient.responsibleSSN)
        textfieldEmployer.setSavedText(patient.responsibleEmployer)
        textfieldWorkPhone.setSavedText(patient.responsibleWorkPhone)
        
    }
    
    func saveValue (){
        patient.responsibleBirthdate = textfieldBirthdate.getText()
        patient.responsibleSSN = textfieldSSN.getText()
        patient.responsibleEmployer = textfieldEmployer.getText()
        patient.responsibleWorkPhone = textfieldWorkPhone.getText()
        
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldSSN.isEmpty && !textfieldSSN.text!.isSocialSecurityNumber{
            self.showAlert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")
        }else if !textfieldWorkPhone.isEmpty && !textfieldWorkPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID WORK PHONE NUMBER")
        }else{
            saveValue()
            YesOrNoAlert.sharedInstance.showWithTitle("DO YOU HAVE DENTAL INSURANCE", button1Title: "YES", button2Title: "NO", completion: { (buttonIndex) in
                if buttonIndex == 1{
                    self.patient.haveInsurance = true
                    let newPatient = newPatientStoryboard.instantiateViewControllerWithIdentifier("NewPatient4VC") as! NewPatient4ViewController
                    newPatient.patient = self.patient
                    //newPatient.isPrimary = true
                    self.navigationController?.pushViewController(newPatient, animated: true)
                    
                }else{
                    self.patient.haveInsurance = false
                    self.patient.primaryInsurance = Insurance()
                    let newPatient = newPatientStoryboard.instantiateViewControllerWithIdentifier("NewPatient7VC") as! NewPatient7ViewController
                    newPatient.patient = self.patient
                    self.navigationController?.pushViewController(newPatient, animated: true)

                }
            })
            
        }
    }
    
}
