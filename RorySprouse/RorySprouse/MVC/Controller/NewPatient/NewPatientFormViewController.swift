//
//  NewPatientFormViewController.swift
//  RorySprouse
//
//  Created by Berlin Raj on 03/01/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatientFormViewController: MCViewController {

    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelBirthdate : UILabel!
    @IBOutlet weak var labelSSN : UILabel!
    @IBOutlet weak var labelAddress : UILabel!
    @IBOutlet weak var labelCity : UILabel!
    @IBOutlet weak var labelState : UILabel!
    @IBOutlet weak var labelZipcode : UILabel!
    @IBOutlet weak var labelHomePhone : UILabel!
    @IBOutlet weak var labelCellPhone : UILabel!
    @IBOutlet weak var labelEmail : UILabel!
    @IBOutlet weak var radioSex : RadioButton!
    @IBOutlet weak var radioMaritialStatus : RadioButton!
    @IBOutlet weak var labelEmployer : UILabel!
    @IBOutlet weak var labelWorkPhone : UILabel!
    @IBOutlet weak var labelReference : UILabel!
    @IBOutlet weak var labelEmergencyName : UILabel!
    @IBOutlet weak var labelEmergencyPhone : UILabel!
    @IBOutlet weak var labelResponsibleName : UILabel!
    @IBOutlet weak var labelResponsibleRelation : UILabel!
    @IBOutlet weak var labelResponsibleAddress : UILabel!
    @IBOutlet weak var labelResponsibleCity : UILabel!
    @IBOutlet weak var labelResponsibleState : UILabel!
    @IBOutlet weak var labelResponsibleZipcode : UILabel!
    @IBOutlet weak var labelResponsibleBirthdate : UILabel!
    @IBOutlet weak var labelResponsibleSSN : UILabel!
    @IBOutlet weak var labelResponsibleEmployer : UILabel!
    @IBOutlet weak var labelResponsibleEmployerWorkPhone : UILabel!
    @IBOutlet weak var labelInsuranceName : UILabel!
    @IBOutlet weak var labelInsuranceBirthdate : UILabel!
    @IBOutlet weak var labelInsuranceSSN : UILabel!
    @IBOutlet weak var labelInsuranceCompany : UILabel!
    @IBOutlet weak var labelInsuranceGroup : UILabel!
    @IBOutlet weak var labelInsuranceAddress : UILabel!
    @IBOutlet weak var labelInsuranceCity : UILabel!
    @IBOutlet weak var labelInsuranceState : UILabel!
    @IBOutlet weak var labelInsuranceZipcode : UILabel!
    @IBOutlet weak var imageviewInsuranceSignature : UIImageView!
    @IBOutlet weak var labelInsuranceDate : UILabel!
    
    @IBOutlet weak var labelReasonVisit : UILabel!
    @IBOutlet weak var labelDentalCleaning : UILabel!
    @IBOutlet weak var labelSpecificConcerns : UILabel!
    @IBOutlet      var arrayButtons1 : [RadioButton]!
    @IBOutlet      var arrayButtons2 : [RadioButton]!
    @IBOutlet      var arrayButtons3 : [RadioButton]!
    @IBOutlet      var arrayButtons4 : [RadioButton]!
    @IBOutlet      var arrayButtons5 : [RadioButton]!
    @IBOutlet      var arrayAllergyButtons : [RadioButton]!
    @IBOutlet weak var labelFloss : UILabel!
    @IBOutlet weak var labelBrush : UILabel!
    @IBOutlet weak var labelCleanYourMouth : UILabel!
    @IBOutlet weak var radioTeethLifetime : RadioButton!
    @IBOutlet weak var labelPhysicinName : UILabel!
    @IBOutlet weak var labelPhysicianLastVisit : UILabel!
    @IBOutlet weak var radioSeriousIllness : RadioButton!
    @IBOutlet weak var labelSeriousIllness : UILabel!
    @IBOutlet weak var radioPregnant : RadioButton!
    @IBOutlet      var labelOthers : UILabel!
    @IBOutlet      var labelMedications : UILabel!
    @IBOutlet weak var imageviewFinancialSignature : UIImageView!
    @IBOutlet weak var labelFinancialDate : UILabel!


    
    //Privacy Practices
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelPrintName: UILabel!
    @IBOutlet weak var signatureView4: UIImageView!
    @IBOutlet weak var labelDate4: UILabel!
    @IBOutlet weak var radioReason: RadioButton!
    @IBOutlet weak var labelOtherReason1: UILabel!
    @IBOutlet weak var labelOtherReason2: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        labelPatientName.text = patient.fullName
        labelBirthdate.text = patient.dateOfBirth
        labelSSN.text = patient.socialSecurityNumber.socialSecurityNumber
        labelAddress.text = patient.address
        labelCity.text = patient.city
        labelState.text = patient.state
        labelZipcode.text = patient.zipcode
        labelHomePhone.text = patient.homephone
        labelCellPhone.text = patient.mobilePhone
        labelEmail.text = patient.email
        radioSex.setSelectedWithTag(patient.genderTag)
        radioMaritialStatus.setSelectedWithTag(patient.maritialStatusTag)
        labelEmployer.text = patient.employerName
        labelWorkPhone.text = patient.workPhone
        labelReference.text = patient.reference
        labelEmergencyName.text = patient.emergencyContactName
        labelEmergencyPhone.text = patient.emergencyContactPhone
        labelResponsibleName.text = patient.responsibleName
        labelResponsibleRelation.text = patient.responsibleRelation
        labelResponsibleAddress.text = patient.responsibleAddress
        labelResponsibleCity.text = patient.responsibleCity
        labelResponsibleState.text = patient.responsibleState
        labelResponsibleZipcode.text = patient.responsibleZipcode
        labelResponsibleBirthdate.text = patient.responsibleBirthdate
        labelResponsibleSSN.text = patient.responsibleSSN
        labelResponsibleEmployer.text = patient.responsibleEmployer
        labelResponsibleEmployerWorkPhone.text = patient.responsibleWorkPhone
        labelInsuranceName.text = patient.primaryInsurance.LegalName
        labelInsuranceBirthdate.text = patient.primaryInsurance.birthdate
        labelInsuranceSSN.text = patient.primaryInsurance.socialSecurity.socialSecurityNumber
        labelInsuranceCompany.text = patient.primaryInsurance.insuranceCompany
        labelInsuranceGroup.text = patient.primaryInsurance.group
        labelInsuranceAddress.text = patient.primaryInsurance.address
        labelInsuranceCity.text = patient.primaryInsurance.city
        labelInsuranceState.text = patient.primaryInsurance.state
        labelInsuranceZipcode.text = patient.primaryInsurance.zipcode
        imageviewInsuranceSignature.image = patient.insuranceSignature
        labelInsuranceDate.text = patient.haveInsurance == true ? patient.dateToday : ""
        
        labelReasonVisit.text = patient.reasonTodayVisit
        labelDentalCleaning.text = patient.lastDentalCleaning
        labelSpecificConcerns.text = patient.specificConcerns
        for btn in arrayButtons1{
            btn.selected = patient.medicalHistory.medicalQuestions[0][btn.tag].selectedOption
        }
        for btn in arrayButtons2{
            btn.selected = patient.medicalHistory.medicalQuestions[1][btn.tag].selectedOption
        }
        for btn in arrayButtons3{
            btn.selected = patient.medicalHistory.medicalQuestions[2][btn.tag].selectedOption
        }
        for btn in arrayButtons4{
            btn.selected = patient.medicalHistory.medicalQuestions[3][btn.tag].selectedOption
        }
        for btn in arrayButtons5{
            btn.selected = patient.medicalHistory.medicalQuestions[4][btn.tag].selectedOption
        }
        for btn in arrayAllergyButtons{
            btn.selected = patient.medicalHistory.allergies[btn.tag].selectedOption
        }

        labelFloss.text = patient.floss
        labelBrush.text = patient.brush
        labelCleanYourMouth.text = patient.cleanMouth
        radioTeethLifetime.setSelectedWithTag(patient.radioTeethLifetimeTag)
        labelPhysicinName.text = patient.physicianName
        labelPhysicianLastVisit.text = patient.physicianLastVisit
        radioSeriousIllness.setSelectedWithTag(patient.seriousIllnessTag)
        labelSeriousIllness.text = patient.seriousIllness
        radioPregnant.setSelectedWithTag(patient.womanRuPregnant)
        labelOthers.text = patient.medicalHistory.medicalQuestions[4][8].answer
        labelMedications.text = patient.listMedications
        imageviewFinancialSignature.image = patient.financialSignature
        labelFinancialDate.text = patient.dateToday
        
        if patient.signRefused == true {
            radioReason.setSelectedWithTag(patient.signRefusalTag)
            patient.signRefusalOther.setTextForArrayOfLabels([labelOtherReason1, labelOtherReason2])
            labelName.text = ""
            labelPrintName.text = ""
            signatureView4.image = nil
            labelDate4.text = ""
        } else {
            radioReason.setSelectedWithTag(0)
            labelOtherReason1.text = ""
            labelOtherReason2.text = ""
            labelName.text = patient.fullName
            labelPrintName.text = patient.fullName
            signatureView4.image = patient.privacyAcknSignature
            labelDate4.text = patient.dateToday
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
