//
//  ViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HomeViewController: MCViewController {
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var tableViewForms: UITableView!
    @IBOutlet weak var labelVersion : UILabel!
    @IBOutlet weak var labelPlace: UILabel!
    
    var selectedForms : [Forms]! = [Forms]()
    var formList : [Forms]! = [Forms]()
    var signRefused: Bool!
    var signRefusalTag: Int!
    var signRefusalOther: String!
//    var signRefusalEffort: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HomeViewController.showCompletionAlert), name: kFormsCompletedNotification, object: nil)
        
        if let text = NSBundle.mainBundle().infoDictionary?[kCFBundleVersionKey as String] as? String {
            labelVersion.text = text
        }
        
        labelPlace.text = kPlace
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(dateChangedNotification), name: kDateChangedNotification, object: nil)
        self.dateChangedNotification()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func dateChangedNotification() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = kCommonDateFormat
        labelDate.text = dateFormatter.stringFromDate(NSDate()).uppercaseString
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if kAppLoginAvailable {
            loginValidation()
        }
        Forms.getAllForms { (isConnectionFailed, forms) -> Void in
            self.formList = forms
            self.tableViewForms.reloadData()
            if isConnectionFailed == true {
                self.showAlert("Your device is not connected to internet. Please go to settings to connect.", buttonTitles: ["Settings", "Cancel"], completion: { (buttonIndex) in
                    if buttonIndex == 0 {
                        let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                        if let url = settingsUrl {
                            UIApplication.sharedApplication().openURL(url)
                        }
                    } else {
                        
                    }
                })
            }
        }
    }
    func loginValidation() {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
            let defaults = NSUserDefaults.standardUserDefaults()
            ServiceManager.loginWithUsername(defaults.valueForKey(kAppLoginUsernameKey) as! String, password: defaults.valueForKey(kAppLoginPasswordKey) as! String) { (success, error) -> (Void) in
                if success {
                    
                } else {
                    if error == nil {
                        
                    } else {
                        dispatch_async(dispatch_get_main_queue(), {
                            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "kApploggedIn")
                            NSUserDefaults.standardUserDefaults().synchronize()
                            (UIApplication.sharedApplication().delegate as! AppDelegate).checkAutologin()
                        })
                    }
                }
            }
        }
    }
    
    @IBAction func buttonActionNext(WithSender sender: AnyObject) {
        self.view.endEditing(true)
        selectedForms.removeAll()
        for (_, form) in formList.enumerate() {
            if form.isSelected == true {
                if form.formTitle == kConsentForms  {
                    for subForm in form.subForms {
                        if subForm.isSelected == true {
                            selectedForms.append(subForm)
                        }
                    }
                } else {
                    selectedForms.append(form)
                }
            }
        }
        
        let patient = MCPatient(forms: selectedForms)
        patient.dateToday = labelDate.text
        patient.signRefused = self.signRefused
        patient.signRefusalOther = self.signRefusalOther
//        patient.signRefusalEffort = self.signRefusalEffort
        patient.signRefusalTag = self.signRefusalTag
        if selectedForms.count > 0 {
            let patientInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientInfoVC") as! PatientInfoViewController
            patientInfoVC.patient = patient
            self.navigationController?.pushViewController(patientInfoVC, animated: true)
        } else {
            self.showAlert("PLEASE SELECT ANY FORM")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    func showCompletionAlert() {
        self.showCustomAlert("PLEASE HANDOVER THE DEVICE BACK TO FRONT DESK")
    }
}


extension HomeViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == consentIndex {
            let subForms = formList[consentIndex].subForms
            for subFrom in subForms {
                subFrom.isSelected = false
            }
            let form = self.formList[consentIndex]
            form.isSelected = !form.isSelected
            var indexPaths : [NSIndexPath] = [NSIndexPath]()
            for (idx, _) in form.subForms.enumerate() {
                let indexPath = NSIndexPath(forRow: consentIndex + 1 + idx, inSection: 0)
                indexPaths.append(indexPath)
            }
            if form.isSelected == true {
                tableView.beginUpdates()
                tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Bottom)
                tableView.endUpdates()
                let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.2 * Double(NSEC_PER_SEC)))
                dispatch_after(delayTime, dispatch_get_main_queue()) {
                    if form.isSelected == true && indexPaths.count > 0 {
                        tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: tableView.numberOfRowsInSection(0) - 1, inSection: 0), atScrollPosition: .Bottom, animated: true)
                    }
                }
            } else {
                tableView.beginUpdates()
                tableView.deleteRowsAtIndexPaths(indexPaths, withRowAnimation: .Bottom)
                tableView.endUpdates()
            }
            tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
            return
        }
        
        var form : Forms!
        if (indexPath.row <= consentIndex) {
            form = formList[indexPath.row]
        } else if formList[consentIndex].isSelected && indexPath.row <= consentIndex + formList[consentIndex].subForms.count {
            form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
        } else {
            form = formList.last
        }
        

//        if !form.isSelected && form.formTitle == kNewPatientSignInForm {
//            form.isSelected = true
//            self.tableViewForms.reloadData()
//        }
        
        if !form.isSelected && form.formTitle == kNewPatientSignInForm {
            HipaaSignatureRefusalPopup.popUpView().showInViewController(self, completion: { (popUpView, refused, textFieldOther, refusalTag) in
                form.isSelected = true
                tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.None)
                popUpView.close()
                self.signRefused = refused
                self.signRefusalTag = refused == false ? 0 : refusalTag
                self.signRefusalOther = refused == false ? "" : refusalTag == 4 ? textFieldOther.text! : ""
                }, error: { (message) in
                    self.showAlert(message)
            })
        } else {
            form.isSelected = !form.isSelected
            tableView.reloadData()
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let cell = tableView.dequeueReusableCellWithIdentifier((indexPath.row <= consentIndex) ? "cellMainForm" : (formList[consentIndex].isSelected && indexPath.row <= consentIndex + formList[consentIndex].subForms.count) ? "cellSubForm" : "cellMainForm") as! FormsTableViewCell
        var form : Forms!
        if (indexPath.row <= consentIndex) {
            form = formList[indexPath.row]
            let height = form.formTitle.heightWithConstrainedWidth(560, font: cell.labelFormName.font) + 24
            return height
        } else if formList[consentIndex].isSelected && indexPath.row <= consentIndex + formList[consentIndex].subForms.count {
            form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
            let height = form.formTitle.heightWithConstrainedWidth(520, font: cell.labelFormName.font) + 24
            return height
        } else {
            form = formList.last!
            let height = form.formTitle.heightWithConstrainedWidth(560, font: cell.labelFormName.font) + 24
            return height
        }
    }
}

extension HomeViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if formList.count > 0 {
            if formList.count > consentIndex {
                return formList[consentIndex].isSelected == true ? formList.count + formList[consentIndex].subForms.count : formList.count
            } else {
                return formList.count
            }
            
            //return formList.count
        } else {
            return 0
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if (indexPath.row <= consentIndex) {
            let cell = tableView.dequeueReusableCellWithIdentifier("cellMainForm", forIndexPath: indexPath) as! FormsTableViewCell
            let form = formList[indexPath.row]
            cell.labelFormName.text = form.formTitle
            cell.imageViewCheckMark.hidden = !form.isSelected
            cell.backgroundColor = UIColor.clearColor()
            cell.contentView.backgroundColor = UIColor.clearColor()
            return cell
        } else if formList[consentIndex].isSelected && indexPath.row <= consentIndex + formList[consentIndex].subForms.count {
            let cell = tableView.dequeueReusableCellWithIdentifier("cellSubForm", forIndexPath: indexPath) as!FormsTableViewCell
            let form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
            cell.labelFormName.text = form.formTitle
            cell.imageViewCheckMark.hidden = !form.isSelected
            cell.backgroundColor = UIColor.clearColor()
            cell.contentView.backgroundColor = UIColor.clearColor()
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("cellMainForm", forIndexPath: indexPath) as! FormsTableViewCell
            let form = formList[consentIndex + 1]
            cell.labelFormName.text = form.formTitle
            cell.imageViewCheckMark.hidden = !form.isSelected
            cell.backgroundColor = UIColor.clearColor()
            cell.contentView.backgroundColor = UIColor.clearColor()
            return cell
        }
    }
    
}
