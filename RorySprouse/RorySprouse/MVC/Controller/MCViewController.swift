//
//  MCViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

let mainStoryBoard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
//let medicalStoryboard = UIStoryboard(name: "MedicalHistory", bundle: NSBundle.mainBundle())
let newPatientStoryboard = UIStoryboard(name: "NewPatient", bundle: NSBundle.mainBundle())
//let consentStoryBoard = UIStoryboard(name: "Consent", bundle: NSBundle.mainBundle())
//let consentStoryBoard2 = UIStoryboard(name: "Consent2", bundle: NSBundle.mainBundle())

class MCViewController: UIViewController {
    
    @IBOutlet var buttonSubmit: MCButton?
    @IBOutlet var buttonBack: MCButton?
    
    @IBOutlet var pdfView: UIScrollView?
    
    var patient: MCPatient!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = .None
        
        configureNavigationButtons()
        // Do any additional setup after loading the view.
    }
    
    func configureNavigationButtons() {
        self.buttonSubmit?.backgroundColor = UIColor.greenColor()
        self.buttonBack?.hidden = self.buttonSubmit == nil && isFromPreviousForm
        
        if self.buttonBack != nil && self.buttonSubmit == nil && isFromPreviousForm {
            self.navigationController?.viewControllers.removeRange(1...self.navigationController!.viewControllers.indexOf(self)! - 1)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var isFromPreviousForm: Bool {
        get {
            return navigationController?.viewControllers.count > 3 ? true : false
        }
    }
    
    @IBAction func buttonBackAction() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func onSubmitButtonPressed() {
        if !Reachability.isConnectedToNetwork() {
            self.showAlert("Your device is not connected to internet. Please go to settings to connect.", buttonTitles: ["Settings", "Cancel"], completion: { (buttonIndex) in
                if buttonIndex == 0 {
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.sharedApplication().openURL(url)
                    }
                } else {
                    
                }
            })
            return
        }
        let pdfManager = PDFManager.sharedInstance()
        pdfManager.authorizeDrive(self) { (success) -> Void in
            if success {
                self.buttonSubmit?.hidden = true
                self.buttonBack?.hidden = true
                
                pdfManager.uploadToGoogleDrive(self.pdfView == nil ? self.view : self.pdfView!, patient: self.patient, completionBlock: { (finished) -> Void in
                    if finished {
                        self.patient.selectedForms.removeFirst()
                        self.gotoNextForm()
                    } else {
                        self.buttonSubmit?.hidden = false
                        self.buttonBack?.hidden = false
                    }
                })
            } else {
                self.buttonSubmit?.hidden = false
                self.buttonBack?.hidden = false
            }
        }
    }
    
    
    func gotoNextForm() {
        let formNames = (patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
        if formNames.contains(kNewPatientSignInForm) {
            let newPatient = newPatientStoryboard.instantiateViewControllerWithIdentifier("NewPatient1VC") as! NewPatient1ViewController
            newPatient.patient = self.patient
            self.navigationController?.pushViewController(newPatient, animated: true)
        }else if formNames.contains(kInsuranceCard) {
            let insuranceCard = mainStoryBoard.instantiateViewControllerWithIdentifier("kCardImageCaptureVC") as! CardImageCaptureVC
            insuranceCard.patient = self.patient
            self.navigationController?.pushViewController(insuranceCard, animated: true)
        } else if formNames.contains(kDrivingLicense) {
            let drivingLicense = mainStoryBoard.instantiateViewControllerWithIdentifier("kCardImageCaptureVC") as! CardImageCaptureVC
            drivingLicense.patient = self.patient
            drivingLicense.isDrivingLicense = true
            self.navigationController?.pushViewController(drivingLicense, animated: true)
        } else {
            NSNotificationCenter.defaultCenter().postNotificationName(kFormsCompletedNotification, object: nil)
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
    }
}
