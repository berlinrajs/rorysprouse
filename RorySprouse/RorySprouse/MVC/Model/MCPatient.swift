//
//  MCPatient.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


class MCPatient: NSObject {
    
    required override init() {
        super.init()
    }
    
    init(forms : [Forms]) {
        super.init()
        self.selectedForms = forms
    }
    
    var selectedForms : [Forms]!
    var firstName : String!
    var lastName : String!
    var initial : String!
    var prefferedName : String!

    var dateOfBirth : String!
    var dateToday : String!
    var dentistName: String!
    
    var parentFirstName: String!
    var parentLastName: String!
    var parentDateOfBirth: String!
    
    var is18YearsOld : Bool {
        return patientAge >= 18
    }
    
    var fullName: String {
        return initial.characters.count > 0 ? firstName + " " + initial + " " + lastName : firstName + " " + lastName
    }
    var patientAge: Int! {
        get {
            if dateOfBirth == nil || dateOfBirth.characters.count == 0 {
                return 0
            }
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = kCommonDateFormat
            
            let numberOfDays = (NSDate().timeIntervalSinceDate(dateFormatter.dateFromString(self.dateOfBirth)!))/(3600 * 24)
            return Int(numberOfDays/365.2425)
        }
    }
    
   // var nickName : String = "N/A"
    var genderTag : Int = 0
    var maritialStatusTag : Int = 0
    var address: String = "N/A"
    var city: String = "N/A"
    var state: String = "N/A"
    var zipcode: String = "N/A"
    var socialSecurityNumber : String = "N/A"
    
    var homephone: String = "N/A"
    var workPhone: String = "N/A"
    var mobilePhone: String = "N/A"
    var email: String = "N/A"
    var employerName : String = "N/A"
   // var reminderOptionTag : Int = 0
    
    var emergencyContactName : String = "N/A"
    var emergencyContactPhone : String = "N/A"
    var reference : String = "N/A"
    
    var primaryInsurance : Insurance = Insurance()
//    var secondaryInsurance : Insurance = Insurance()
    var haveInsurance : Bool = false
    
    var insuranceSignature : UIImage = UIImage()
    var medicalSignature: UIImage = UIImage()
    var financialSignature: UIImage = UIImage()
    
    var responsibleName : String = "N/A"
    var responsibleRelation : String = "N/A"
    var responsibleAddress : String = "N/A"
    var responsibleCity : String = "N/A"
    var responsibleState : String = "N/A"
    var responsibleZipcode : String = "N/A"
   // var responsiblePhone : String = "N/A"
    
    var responsibleBirthdate : String = "N/A"
    var responsibleSSN : String = "N/A"
    var responsibleEmployer : String = "N/A"
    var responsibleWorkPhone : String = "N/A"
    
    var reasonTodayVisit : String = "N/A"
    var lastDentalCleaning : String = "N/A"
    var specificConcerns : String = "N/A"
    var flossTag : Int = 0
    var brushTag : Int = 0
    var floss : String = "N/A"
    var brush : String = "N/A"
    var cleanMouth : String = "N/A"
    var radioTeethLifetimeTag : Int = 0
    
    var womanRuPregnant : Int! = 0
    var listMedications: String = "N/A"
    var physicianName : String = "N/A"
    var physicianLastVisit : String = "N/A"
    var seriousIllnessTag : Int = 2
    var seriousIllness : String = "N/A"
    
    
    //PrivacyPractices Acknowledgement
    var signRefused: Bool!
    var signRefusalTag: Int!
    var signRefusalOther: String!
//    var signRefusalEffort: String!
    var privacyAcknSignature: UIImage!
    var refusalName: String!
    var refusalCapacity: String!
    
    var medicalHistory : MedicalHistory = MedicalHistory()
    
}

class Insurance: NSObject {
    
    var LegalName : String = "N/A"
    var birthdate : String = "N/A"
    var socialSecurity : String = "N/A"
    var insuranceCompany : String = "N/A"
    var group : String = "N/A"
    var address : String = "N/A"
    var city : String = "N/A"
    var state : String = "N/A"
    var zipcode : String = "N/A"
}

class MedicalHistory : NSObject{
    
    var medicalQuestions : [[MCQuestion]]!
    var allergies : [MCQuestion]!
    
    required override init() {
        super.init()
        let quest1: [String] = ["Loose teeth or broken fillings",
                                "Food collection between teeth",
                                "Periodontal treatment",
                                "Grinding teeth",
                                "Chew on one side of mouth",
                                "Sensitivity to heat",
                                "Bleeding Gums",
                                "Smoking/Tobacco",
                                "Orthodontics",
                                "Clicking or popping jaw",
                                "Mouth breathing"]
        
        let quest2: [String] = ["Sores or growth mouth",
                                "Sensitivity to cold",
                                "Pain around ear",
                                "Gums swollen or tender",
                                "Sensitivity to sweets",
                                "Dry Mouth",
                                "Mouth pain, brushing",
                                "Jaw pain or tenderness",
                                "Sensitivity biting",
                                "Bad Breath"]
        
        let quest3: [String] = ["Mitral Valve Prolapse",
                                "Congenital Heart Lesions",
                                "Hepatitis",
                                "Scarlet Fever",
                                "Arthritis, Rheumatism",
                                "Cortisone Treatment",
                                "Shortness of Breath",
                                "Artificial Heart Valves",
                                "Cough, Persistent",
                                "High Blood Pressure",
                                "Artificial Joints",
                                "Heart Murmur",
                                "HIV/AIDS",
                                "Stroke"]
        
        let quest4: [String] = ["Asthma",
                                "Diabetes",
                                "Epilepsy",
                                "Kidney Disease",
                                "Fainting",
                                "Liver Disease",
                                "Rheumatic Fever",
                                "Back Problems",
                                "Venereal Disease",
                                "Circulatory Problems",
                                "Blood Disease",
                                "Anemia",
                                "Respiratory Disease",
                                "Cancer"]

        
        let quest5: [String] = ["Chronic Headaches",
                                "Pacemaker",
                                "Tuberculosis",
                                "Chemical Dependency",
                                "Glaucoma",
                                "Radiation Treatment",
                                "Chemotherapy",
                                "Hemophilia",
                                "Other"]


        
        self.medicalQuestions = [MCQuestion.getArrayOfQuestions(quest1),MCQuestion.getArrayOfQuestions(quest2),MCQuestion.getArrayOfQuestions(quest3),MCQuestion.getArrayOfQuestions(quest4),MCQuestion.getArrayOfQuestions(quest5)]
        self.medicalQuestions[4][8].isAnswerRequired = true
        
        let allergy1: [String] = ["Local Anesthetics",
                                "Penicillin or other antibiotics",
                                "Codeine",
                                "Aspirin or Motrin",
                                "Latex Gloves"]
        
        self.allergies = MCQuestion.getArrayOfQuestions(allergy1)



    }

}

