//
//  MCQuestion.swift
//  ProDental
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MCQuestion: NSObject {
    var question : String!
    var isAnswerRequired : Bool!
    var answer : String?
    var selectedIndex : Int = 1
    var selectedOption : Bool! {
        didSet {
            if selectedOption == false {
                self.answer = nil
            }
        }
    }
    init(question: String) {
        super.init()
        self.question = question
        self.isAnswerRequired = false
        self.selectedOption = false
    }
    
    init(dict : NSDictionary) {
        super.init()
        self.question = dict["question"] as! String
        self.selectedOption = false
        self.isAnswerRequired = (dict["verification"] as! String) == "Yes"
    }
    
    class func getObjects (arrayResult : NSArray) -> [MCQuestion] {
        var questions  = [MCQuestion]()
        for dict in arrayResult {
            let obj = MCQuestion(dict: dict as! NSDictionary)
            questions.append(obj)
        }
        return questions
    }
    
    

    class func getArrayOfQuestions (questions : [String]) -> [MCQuestion] {
        var arrayQuestions  = [MCQuestion]()
        for quest in questions {
            let obj = MCQuestion(question: quest)
            arrayQuestions.append(obj)
        }
        return arrayQuestions
    }

    
    class func getJSONObject(responseString : String) -> AnyObject? {
        do {
            let object = try NSJSONSerialization.JSONObjectWithData(responseString.dataUsingEncoding(NSUTF8StringEncoding)!, options: NSJSONReadingOptions.AllowFragments)
            return object
        } catch {
            return nil
        }
    }
}
