//
//  Forms.swift
//  WestgateSmiles
//
//  Created by samadsyed on 2/18/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class Forms: NSObject {
    
    var formTitle : String!
    var subForms : [Forms]!
    var isSelected : Bool!
    var index : Int!
    var isToothNumberRequired : Bool!
    var toothNumbers : String!
    
    init(formDetails : NSDictionary) {
        super.init()
        self.isSelected = false
    }
    
    override init() {
        super.init()
    }
    
    class func getAllForms (completion :(isConnectionfailed: Bool, forms : [Forms]?) -> Void) {
        let isConnected = Reachability.isConnectedToNetwork()
        let forms = [kNewPatientSignInForm, kInsuranceCard, kDrivingLicense]
        
        let formObj = getFormObjects(forms, isSubForm: false)
        completion(isConnectionfailed: isConnected ? false : true, forms : formObj)
    }
    

    private class func getFormObjects (forms : [String], isSubForm : Bool) -> [Forms] {
        var formList : [Forms]! = [Forms]()
        for (idx, form) in forms.enumerate() {
            let formObj = Forms()
            formObj.isSelected = false
            formObj.index = isSubForm ? idx + 5 : idx
            formObj.formTitle = form
             formObj.isToothNumberRequired = toothNumberRequired.contains(form)
            if formObj.formTitle == kConsentForms {
                formObj.subForms = getFormObjects([], isSubForm:  true)
            }
            
//            if formObj.formTitle == kFeedBack {
//               formObj.index = forms.count + formList[consentIndex].subForms.count + 1
//            }
            
            formList.append(formObj)
        }
        return formList
    }
    
}
