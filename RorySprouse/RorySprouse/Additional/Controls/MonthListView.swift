//
//  MonthListView.swift
//  MConsentForms
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class MonthListView: UIView {
    var textField: UITextField!
    var pickerView: UIPickerView!
    var toolbar: UIToolbar!
    
    let dictMonth = ["JAN": "JANUARY",
                     "FEB": "FEBRUARY",
                     "MAR": "MARCH",
                     "APR": "APRIL",
                     "MAY": "MAY",
                     "JUN": "JUNE",
                     "JUL": "JULY",
                     "AUG": "AUGUST",
                     "SEP": "SEPTEMBER",
                     "OCT": "OCTOBER",
                     "NOV": "NOVEMBER",
                     "DEC": "DECEMBER"]
    let arrayKeys = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.pickerView = UIPickerView(frame: CGRectMake(0, 0, screenSize.width, 260))
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.pickerView.showsSelectionIndicator = true
        
        self.toolbar = UIToolbar(frame: CGRectMake(0, 0, screenSize.width, 44))
        
        let barbuttonDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(donePressed))
        barbuttonDone.tintColor = UIColor.blackColor()
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil), barbuttonDone]
        
        self.addSubview(pickerView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func donePressed() {
        textField.text = arrayKeys[pickerView.selectedRowInComponent(0)]
        textField.resignFirstResponder()
    }
    class func addMonthListForTextField(textField: UITextField) {
        let monthListView = MonthListView(frame: CGRectMake(0, 0, screenSize.width, 260))
        textField.inputView = monthListView
        textField.inputAccessoryView = monthListView.toolbar
        monthListView.pickerView.reloadAllComponents()
        
        monthListView.textField = textField
    }
}
extension MonthListView: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayKeys.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dictMonth[arrayKeys[row]]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        textField.text = arrayKeys[row]
    }
}
