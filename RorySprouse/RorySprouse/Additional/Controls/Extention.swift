//
//  Extention.swift
//  WestgateSmiles
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

public let screenSize = UIScreen.mainScreen().bounds


public func getText(text : String) -> String {
    return "  \(text)  "
}

extension UIViewController {
    
    func showCustomAlert(message: String) {
        CustomAlert.alertView().showWithTitle(message) { 
            
        }
    }
    
    func showAlert(title: String, message : String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Destructive) { (action) -> Void in
            
        }
        alertController.addAction(alertOkAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    func showAlert(message : String) {
        let alertController = UIAlertController(title: kAppName, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Destructive) { (action) -> Void in
            
        }
        alertController.addAction(alertOkAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    func showAlert(message : String, completion: () -> Void) {
        let alertController = UIAlertController(title: kAppName, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Destructive) { (action) -> Void in
            completion()
        }
        alertController.addAction(alertOkAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    func showAlert(message : String, buttonTitles: [String], completion: (buttonIndex: Int) -> Void) {
        let alertController = UIAlertController(title: kAppName, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        for (idx, title) in buttonTitles.enumerate() {
            let alertOkAction = UIAlertAction(title: title.uppercaseString, style: UIAlertActionStyle.Destructive) { (action) -> Void in
                completion(buttonIndex: idx)
            }
            alertController.addAction(alertOkAction)
        }
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}


extension NSError {
    
    convenience init(errorMessage : String) {
        self.init(domain: "Error", code: 101, userInfo: [NSLocalizedDescriptionKey : errorMessage])
    }
    
}

extension UILabel {
    func setAttributedText() {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.Justified
        let attributedString = NSAttributedString(string: self.text!,
            attributes: [
                NSParagraphStyleAttributeName: paragraphStyle,
                NSBaselineOffsetAttributeName: NSNumber(float: 0),
                NSFontAttributeName : self.font
            ])
        self.attributedText = attributedString
    }
}


extension UITextField {
    
    func getText() -> String {
        if self.isEmpty{
            return "N/A"
        }
        return self.text!
    }
    
    func setSavedText(text : String) {
        if text == "N/A"{
            self.text = ""
        }else{
            self.text = text
        }
    }

    var isEmpty : Bool {
        return self.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).lengthOfBytesUsingEncoding(NSUTF8StringEncoding) == 0
    }
    
    func formatPhoneNumber(range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacterFromSet(NSCharacterSet(charactersInString: "01234567890").invertedSet)?.last != nil {
            return false
        }
        let newRange = self.text!.startIndex.advancedBy(range.location)..<self.text!.startIndex.advancedBy(range.location + range.length)
        let newString = self.text!.stringByReplacingCharactersInRange(newRange, withString: string)
        
        let components = newString.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet)
        
        let decimalString : String = components.joinWithSeparator("")
        let length = decimalString.characters.count
        let decimalStr = decimalString as NSString
        
        
        let hasLeadingOne = length > 0 && decimalStr.characterAtIndex(0) == (1 as unichar)
        if length == 0 || (length > 10 && !hasLeadingOne) || length > 11
        {
            let newLength = self.text!.characters.count + string.characters.count - range.length as Int
            return (newLength > 10) ? false : true
        }
        var index = 0 as Int
        let formattedString = NSMutableString()
        
        if hasLeadingOne
        {
            formattedString.appendString("1 ")
            index += 1
        }
        if (length - index) > 3
        {
            let areaCode = decimalStr.substringWithRange(NSMakeRange(index, 3))
            formattedString.appendFormat("(%@)", areaCode)
            index += 3
        }
        if length - index > 3
        {
            let prefix = decimalStr.substringWithRange(NSMakeRange(index, 3))
            formattedString.appendFormat("%@-", prefix)
            index += 3
        }
        
        let remainder = decimalStr.substringFromIndex(index)
        formattedString.appendString(remainder)
        self.text = formattedString as String
        return false
    }
    
    func formatZipCode(range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacterFromSet(NSCharacterSet(charactersInString: "01234567890").invertedSet)?.last != nil {
            return false
        }
        let newRange = self.text!.startIndex.advancedBy(range.location)..<self.text!.startIndex.advancedBy(range.location + range.length)
        let newString = self.text!.stringByReplacingCharactersInRange(newRange, withString: string)
        if newString.characters.count > 5 {
            return false
        }
        return true
    }
    
    func formatNumbers(range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacterFromSet(NSCharacterSet(charactersInString: "01234567890").invertedSet)?.last != nil {
            return false
        }
        let newRange = self.text!.startIndex.advancedBy(range.location)..<self.text!.startIndex.advancedBy(range.location + range.length)
        let newString = self.text!.stringByReplacingCharactersInRange(newRange, withString: string)
        if newString.characters.count > 9 {
            return false
        }
        return true
    }
    
    func formatAmount(range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacterFromSet(NSCharacterSet(charactersInString: "01234567890").invertedSet)?.last != nil {
            return false
        }
        let newRange = self.text!.startIndex.advancedBy(range.location)..<self.text!.startIndex.advancedBy(range.location + range.length)
        let newString = self.text!.stringByReplacingCharactersInRange(newRange, withString: string).stringByReplacingOccurrencesOfString("$ ", withString: "").stringByReplacingOccurrencesOfString(",", withString: "")
        
        self.setAmountText(newString)
        return false
    }
    
    func setAmountText(text: String) {
        var amountText = ""
        for (idx, char) in text.characters.reverse().enumerate() {
            if idx % 3 == 0 && idx != 0 {
                amountText = "," + amountText
            }
            amountText = "\(char)" + amountText
        }
        if !amountText.hasPrefix("$ ") && amountText.characters.count > 0 {
            amountText = "$ " + amountText
            self.text = amountText
        } else {
            self.text = amountText
        }
    }
    
    func formatExt(range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacterFromSet(NSCharacterSet(charactersInString: "01234567890").invertedSet)?.last != nil {
            return false
        }
        let newRange = self.text!.startIndex.advancedBy(range.location)..<self.text!.startIndex.advancedBy(range.location + range.length)
        let newString = self.text!.stringByReplacingCharactersInRange(newRange, withString: string)
        if newString.characters.count > 3 {
            return false
        }
        return true
    }
    func formatMiddleName(range: NSRange, string: String) -> Bool {
        let newRange = self.text!.startIndex.advancedBy(range.location)..<self.text!.startIndex.advancedBy(range.location + range.length)
        let newString = self.text!.stringByReplacingCharactersInRange(newRange, withString: string).stringByTrimmingCharactersInSet(NSCharacterSet.letterCharacterSet().invertedSet)
        if newString.characters.count > 1 {
            return false
        } else {
            text = newString
        }
        return false
    }
    
    func formatFamilyMembersCount(range: NSRange, string: String) -> Bool{
        if string.rangeOfCharacterFromSet(NSCharacterSet(charactersInString: "01234567890").invertedSet)?.last != nil {
            return false
        }
        let newRange = self.text!.startIndex.advancedBy(range.location)..<self.text!.startIndex.advancedBy(range.location + range.length)
        let newString = self.text!.stringByReplacingCharactersInRange(newRange, withString: string)
        if newString.characters.count > 2 {
            return false
        }
        return true
    }
    func formatNumbers(range: NSRange, string: String, count : Int, limit: Int?) -> Bool {
        if string.rangeOfCharacterFromSet(NSCharacterSet(charactersInString: "01234567890").invertedSet)?.last != nil {
            return false
        }
        let newRange = self.text!.startIndex.advancedBy(range.location)..<self.text!.startIndex.advancedBy(range.location + range.length)
        let newString = self.text!.stringByReplacingCharactersInRange(newRange, withString: string)
        if newString.characters.count > count {
            return false
        }
        if limit != nil && limit > 0 {
            return Int(newString) <= limit
        }
        return true
    }
    
    
    func formatDate(range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacterFromSet(NSCharacterSet(charactersInString: "01234567890").invertedSet)?.last != nil {
            return false
        }
        let newRange = self.text!.startIndex.advancedBy(range.location)..<self.text!.startIndex.advancedBy(range.location + range.length)
        let newString = self.text!.stringByReplacingCharactersInRange(newRange, withString: string)
        if newString.characters.count > 2 {
            return false
        }
        //        let maximumDateForMonth: [String: Int] = ["": 31, "JAN": 31, "FEB": 29, "MAR": 31 ,"APR": 30 ,"MAY": 31 ,"JUN": 30 ,"JUL": 31 ,"AUG": 31 ,"SEP": 30 ,"OCT": 31 ,"NOV": 30 ,"DEC": 31]
        if Int(newString) > 31 {
            return false
        }
        return true
    }
    

    func formatAmericanCreditCardNumber(range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacterFromSet(NSCharacterSet(charactersInString: "01234567890").invertedSet)?.last != nil {
            return false
        }
        let newRange = self.text!.startIndex.advancedBy(range.location)..<self.text!.startIndex.advancedBy(range.location + range.length)
        let newString = self.text!.stringByReplacingCharactersInRange(newRange, withString: string)
        
        let components = newString.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet)
        
        let decimalString : String = components.joinWithSeparator("")
        let length = decimalString.characters.count
        let decimalStr = decimalString as NSString
        
        
        if length == 0 || length > 15
        {
            let newLength = self.text!.characters.count + string.characters.count - range.length as Int
            return (newLength > 15) ? false : true
        }
        var index = 0 as Int
        let formattedString = NSMutableString()
        
        
        if (length - index) > 5
        {
            let areaCode = decimalStr.substringWithRange(NSMakeRange(index, 5))
            formattedString.appendFormat("%@ ", areaCode)
            index += 5
        }
        if length - index > 5
        {
            let prefix = decimalStr.substringWithRange(NSMakeRange(index, 5))
            formattedString.appendFormat("%@ ", prefix)
            index += 5
        }
        if length - index > 5
        {
            let prefix = decimalStr.substringWithRange(NSMakeRange(index, 5))
            formattedString.appendFormat("%@ ", prefix)
            index += 5
        }
        
        let remainder = decimalStr.substringFromIndex(index)
        formattedString.appendString(remainder)
        self.text = formattedString as String
        return false
    }
    
    func formatCreditCardNumber(range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacterFromSet(NSCharacterSet(charactersInString: "01234567890").invertedSet)?.last != nil {
            return false
        }
        let newRange = self.text!.startIndex.advancedBy(range.location)..<self.text!.startIndex.advancedBy(range.location + range.length)
        let newString = self.text!.stringByReplacingCharactersInRange(newRange, withString: string)
        
        let components = newString.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet)
        
        let decimalString : String = components.joinWithSeparator("")
        let length = decimalString.characters.count
        let decimalStr = decimalString as NSString
        
        
        if length == 0 || length > 16
        {
            let newLength = self.text!.characters.count + string.characters.count - range.length as Int
            return (newLength > 16) ? false : true
        }
        var index = 0 as Int
        let formattedString = NSMutableString()
        
        
        if (length - index) > 4
        {
            let areaCode = decimalStr.substringWithRange(NSMakeRange(index, 4))
            formattedString.appendFormat("%@ ", areaCode)
            index += 4
        }
        if length - index > 4
        {
            let prefix = decimalStr.substringWithRange(NSMakeRange(index, 4))
            formattedString.appendFormat("%@ ", prefix)
            index += 4
        }
        if length - index > 4
        {
            let prefix = decimalStr.substringWithRange(NSMakeRange(index, 4))
            formattedString.appendFormat("%@ ", prefix)
            index += 4
        }
        
        let remainder = decimalStr.substringFromIndex(index)
        formattedString.appendString(remainder)
        self.text = formattedString as String
        return false
    }

    
    
    func formatLicense(range: NSRange, string: String, count : Int) -> Bool {
        let newRange = self.text!.startIndex.advancedBy(range.location)..<self.text!.startIndex.advancedBy(range.location + range.length)
        let newString = self.text!.stringByReplacingCharactersInRange(newRange, withString: string)
        if newString.characters.count > count {
            return false
        }
        return true
    }
    func formatToothNumbers(range: NSRange, string: String) -> Bool {
        if string.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) == 0 {
            return true
        }
        if string.rangeOfCharacterFromSet(NSCharacterSet(charactersInString: "01234567890,").invertedSet)?.last != nil {
            return false
        }
        
        let newRange = self.text!.startIndex.advancedBy(range.location)..<self.text!.startIndex.advancedBy(range.location + range.length)
        let textFieldString = self.text!.stringByReplacingCharactersInRange(newRange, withString: string)
        let textString = textFieldString.componentsSeparatedByString(",")
        
        if textFieldString.characters.count > 2 {
            let lastString = textFieldString.substringFromIndex(textFieldString.startIndex.advancedBy(textFieldString.characters.count - 1))
            let lastTwoStrings = textFieldString.substringFromIndex(textFieldString.startIndex.advancedBy(textFieldString.characters.count - 2))
            let lastThreeStrings = textFieldString.substringFromIndex(textFieldString.startIndex.advancedBy(textFieldString.characters.count - 3))
            
            if lastTwoStrings == ",," {
                return false
            }
            if lastString == "," && lastThreeStrings.componentsSeparatedByString(",").count == 3 {
                let requiredString = textFieldString.substringToIndex(textFieldString.startIndex.advancedBy(textFieldString.characters.count - 2)) + "0" + lastTwoStrings
                self.text = requiredString
                return false
            }
            
        } else {
            if textFieldString.characters.count == 2 {
                let lastString = textFieldString.substringFromIndex(textFieldString.startIndex.advancedBy(textFieldString.characters.count - 1))
                if lastString == "," {
                    self.text = "0" + textFieldString
                    return false
                }
            }
            if textFieldString == "," {
                return false
            }
        }
        for text in textString {
            if text == "0" {
                return true
            }
            if text == "00" {
                return false
            }
            if Int(text) > 35 {
                return false
            }
        }
        return true
    }
}

extension UITextView {
    var isEmpty : Bool {
        return self.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).lengthOfBytesUsingEncoding(NSUTF8StringEncoding) == 0
    }
}



extension String {
    
    var fileName : String {
        return self.stringByReplacingOccurrencesOfString(" - ", withString: "-").stringByReplacingOccurrencesOfString(" ", withString: "_").stringByReplacingOccurrencesOfString("/", withString: "_OR_").stringByReplacingOccurrencesOfString("&", withString: "_AND_").stringByReplacingOccurrencesOfString("\'", withString: "")
    }
    
    public func rangeOfText(text : String) -> NSRange {
        return NSMakeRange(self.characters.count - text.characters.count, text.characters.count)
    }
    
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.max)
        let boundingBox = self.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox.height
    }
    
    var isValidEmail : Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(self)
    }
    
    var socialSecurityNumber: String {
        get {
            if self.characters.count != 9 {
                return self
            }
            var ssn: String = ""
            for char in self.characters {
                ssn.append(char)
                if ssn.characters.count == 3 || ssn.characters.count == 6 {
                    ssn = ssn + "-"
                }
            }
            return ssn
        }
    }
    
    var isValidYear: Bool {
        if self.characters.count != 4 {
            return false
        }
        let components = NSCalendar.currentCalendar().components(NSCalendarUnit.Year, fromDate: NSDate())
        if Int(self) > components.year {
            return false
        }
        return true
    }
    
    
    var isCreditCard: Bool {
        let charcter  = NSCharacterSet(charactersInString: "+0123456789").invertedSet
        let inputString:NSArray = self.componentsSeparatedByCharactersInSet(charcter)
        let filtered = inputString.componentsJoinedByString("")
        return filtered.characters.count == 16
    }
    var isAmericanCreditCard: Bool {
        let charcter  = NSCharacterSet(charactersInString: "+0123456789").invertedSet
        let inputString:NSArray = self.componentsSeparatedByCharactersInSet(charcter)
        let filtered = inputString.componentsJoinedByString("")
        return filtered.characters.count == 15
    }

    
    var isPhoneNumber: Bool {
        let charcter  = NSCharacterSet(charactersInString: "0123456789").invertedSet
        let inputString:NSArray = self.componentsSeparatedByCharactersInSet(charcter)
        let filtered = inputString.componentsJoinedByString("")
        return filtered.characters.count == 10
    }
    var phoneNumber: String {
        let charcter  = NSCharacterSet(charactersInString: "0123456789").invertedSet
        let inputString:NSArray = self.componentsSeparatedByCharactersInSet(charcter)
        let filtered = inputString.componentsJoinedByString("")
        return filtered
    }
    
    var isZipCode: Bool {
        let charcter  = NSCharacterSet(charactersInString: "0123456789").invertedSet
        let inputString:NSArray = self.componentsSeparatedByCharactersInSet(charcter)
        let filtered = inputString.componentsJoinedByString("")
        return filtered.characters.count == 5
    }
    
    var isSocialSecurityNumber: Bool {
        let charcter  = NSCharacterSet(charactersInString: "0123456789").invertedSet
        let inputString:NSArray = self.componentsSeparatedByCharactersInSet(charcter)
        let filtered = inputString.componentsJoinedByString("")
        return filtered.characters.count == 9
    }
    var isValidMRN: Bool {
        let charcter  = NSCharacterSet(charactersInString: "0123456789").invertedSet
        let inputString:NSArray = self.componentsSeparatedByCharactersInSet(charcter)
        let filtered = inputString.componentsJoinedByString("")
        return filtered.characters.count >= 7 && filtered.characters.count <= 8
    }
    
    var isValidExt: Bool {
        let charcter  = NSCharacterSet(charactersInString: "0123456789").invertedSet
        let inputString:NSArray = self.componentsSeparatedByCharactersInSet(charcter)
        let filtered = inputString.componentsJoinedByString("")
        return filtered.characters.count == 3
    }
    
    func setTextForArrayOfLabels(arrayOfLabels: [UILabel]) {
        
        if arrayOfLabels.count == 0 {
            return
        }
        
        let wordArray = self.componentsSeparatedByString(" ")
        
        var textToCheck: NSString = ""
        
        for string in wordArray {
            let previousLength = textToCheck.length
            let label = arrayOfLabels[0]
            textToCheck = textToCheck.length == 0 ? (textToCheck as String) + string : (textToCheck as String) + " " + string
            
            let size = textToCheck.sizeWithAttributes([NSFontAttributeName: label.font])
            if size.height > CGRectGetHeight(label.frame) || size.width > CGRectGetWidth(label.frame) {
                var array = arrayOfLabels
                array.removeFirst()
                ((self as NSString).stringByReplacingCharactersInRange(NSMakeRange(0, previousLength), withString: "").stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())).setTextForArrayOfLabels(array)
                return
            } else {
                label.text = textToCheck as String
            }
        }
    }
  
}

extension NSAttributedString {
    func heightWithConstrainedWidth(width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.max)
        let boundingBox = self.boundingRectWithSize(constraintRect, options: [.UsesLineFragmentOrigin, .UsesFontLeading], context: nil)
        return ceil(boundingBox.height) + 10
    }

}

