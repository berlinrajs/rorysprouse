//
//  MCTextField.swift
//  WestgateSmiles
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MCTextField: UITextField {

    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        layer.borderColor = enabled ? borderColor.CGColor : borderColor.colorWithAlphaComponent(0.2).CGColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: enabled ? placeHolderColor : placeHolderColor.colorWithAlphaComponent(0.2)])
        tintColor = enabled ? placeHolderColor : placeHolderColor.colorWithAlphaComponent(0.2)
        clipsToBounds = true
        
        self.autocorrectionType = UITextAutocorrectionType.No
        self.spellCheckingType = UITextSpellCheckingType.No
        self.autocapitalizationType = UITextAutocapitalizationType.AllCharacters
        
        self.prepareTextField()
    }
    
    private var count : Int?
    private var limit : Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if delegate == nil {
            delegate = self
        }
    }
    
    var borderColor: UIColor = UIColor.whiteColor() {
        didSet {
            layer.borderColor = borderColor.CGColor
        }
    }
    
    var cornerRadius: CGFloat = 3.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }
    
    var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    var placeHolderColor: UIColor = UIColor.whiteColor().colorWithAlphaComponent(0.5) {
        didSet {
            attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: placeHolderColor])
            tintColor = placeHolderColor
        }
    }
    
    override var placeholder: String? {
        didSet {
            attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: placeHolderColor])
            tintColor = placeHolderColor
        }
    }
    
    func setNumberFormatWithCount(count: Int, limit: Int) {
        self.textFormat = .Number
        self.count = count
        self.limit = limit
    }
    
    @IBInspectable var textFormat: TextFormat = TextFormat.Default
    func prepareTextField() {
        switch self.textFormat {
        case .Default:
            self.keyboardType = UIKeyboardType.Default
        case .SocialSecurity:
            self.secureTextEntry = true
            self.keyboardType = UIKeyboardType.NumbersAndPunctuation
            self.count = 9
        case .ToothNumber:
            self.keyboardType = UIKeyboardType.NumbersAndPunctuation
        case .Phone:
            self.keyboardType = UIKeyboardType.NumbersAndPunctuation
        case .Zipcode:
            self.keyboardType = UIKeyboardType.NumbersAndPunctuation
        case .Number:
            self.keyboardType = UIKeyboardType.NumbersAndPunctuation
            if self.count == nil {
                self.count = 100
            }
        case .Date:
            self.keyboardType = UIKeyboardType.NumbersAndPunctuation
            self.count = 2
            self.limit = 31
        case .MiddleInitial:
            self.keyboardType = UIKeyboardType.Default
        case .Month:
            MonthListView.addMonthListForTextField(self)
        case .Year:
            self.keyboardType = UIKeyboardType.NumbersAndPunctuation
            self.count = 4
        case .Time:
            DateInputView.addTimePickerForTextField(self)
        case .DateInCurrentYear:
            DateInputView.addDatePickerForTextField(self)
        case .DateIn1980:
            DateInputView.addDatePickerForDateOfBirthTextField(self)
        case .State:
            StateListView.addStateListForTextField(self)
        case .Amount:
            self.keyboardType = UIKeyboardType.NumbersAndPunctuation
        case .ExtensionCode:
            self.keyboardType = UIKeyboardType.NumbersAndPunctuation
        case .Email:
            self.keyboardType = UIKeyboardType.EmailAddress
            self.autocapitalizationType = UITextAutocapitalizationType.None
        case .SecureText:
            self.secureTextEntry = true
        case .AlphaNumeric:
            self.keyboardType = UIKeyboardType.NumbersAndPunctuation
        case .NumbersWithoutValidation:
            self.keyboardType = UIKeyboardType.NumbersAndPunctuation

        //use "setNumberFormatWithCount" function to validate other numeric values
        }
    }
}
extension MCTextField: UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if self.textFormat == .Phone {
            return textField.formatPhoneNumber(range, string: string)
        } else if self.textFormat == .Zipcode {
            return textField.formatZipCode(range, string: string)
        } else if self.textFormat == .Number || self.textFormat == .SocialSecurity || self.textFormat == .Year || self.textFormat == .Date {
            return textField.formatNumbers(range, string: string, count: count!, limit: limit)
        } else if textFormat == .Amount {
            return textField.formatAmount(range, string: string)
        } else if textFormat == .ExtensionCode {
            return textField.formatExt(range, string: string)
        } else if self.textFormat == .MiddleInitial {
            return textField.formatMiddleName(range, string: string)
        } else if self.textFormat == .ToothNumber {
            return textField.formatToothNumbers(range, string: string)
        }
        return true
    }
}
