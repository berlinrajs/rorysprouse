//
//  MCTextView.swift
//  ProDental
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MCTextView: UITextView {
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        
        layer.borderColor = borderColor.CGColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        clipsToBounds = true
        
        self.autocorrectionType = UITextAutocorrectionType.No
        self.spellCheckingType = UITextSpellCheckingType.No
        self.autocapitalizationType = UITextAutocapitalizationType.AllCharacters
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        if delegate == nil {
            delegate = self
        }
    }
    
    var borderColor: UIColor = UIColor.whiteColor() {
        didSet {
            layer.borderColor = borderColor.CGColor
        }
    }
    
    var cornerRadius: CGFloat = 3.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }
    
    var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    var textValue: String? {
        get {
            self.delegate?.textViewDidBeginEditing?(self)
            let returnString = self.text
            self.delegate?.textViewDidEndEditing?(self)
            return returnString
        } set {
            self.delegate?.textViewDidBeginEditing?(self)
            self.text = newValue
            self.delegate?.textViewDidEndEditing?(self)
        }
    }
    
    @IBInspectable var characterCount: Int! = 0
    
    @IBInspectable var placeholder: String? = nil {
        didSet {
            if placeholder?.characters.count > 0 {
                self.text = placeholder
                self.textColor = self.placeholderColor
            }
        }
    }
    @IBInspectable var defaultTextColor: UIColor = UIColor.blackColor() {
        didSet {
            if self.text != self.placeholder {
                self.textColor = defaultTextColor
            }
        }
    }
    
    @IBInspectable var placeholderColor: UIColor = UIColor.lightGrayColor() {
        didSet {
            if self.text == self.placeholder {
                self.textColor = placeholderColor
            }
        }
    }
    
    override var isEmpty : Bool {
        return text == placeholder ? true : super.isEmpty
    }
}
extension MCTextView: UITextViewDelegate {
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == "" {
            textView.text = placeholder
            textView.textColor = placeholderColor
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        let newRange = self.text!.startIndex.advancedBy(range.location)..<self.text!.startIndex.advancedBy(range.location + range.length)
        let newString = self.text!.stringByReplacingCharactersInRange(newRange, withString: text)
        
        return characterCount == 0 || newString.characters.count <= characterCount
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == placeholder {
            textView.text = ""
            textView.textColor = self.defaultTextColor
        }
    }
}
