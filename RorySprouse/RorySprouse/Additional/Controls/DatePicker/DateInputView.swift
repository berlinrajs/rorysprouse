//
//  DateInputView.swift
//  Secure Dental
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class DateInputView: UIView {
    var textField: UITextField!
    var datePicker: UIDatePicker!
    var toolbar: UIToolbar!
    
    var arrayStates: [String]!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.datePicker = UIDatePicker(frame: CGRectMake(0, 0, screenSize.width, 260))
        datePicker.addTarget(self, action: #selector(datePickerDateChanged(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        self.toolbar = UIToolbar(frame: CGRectMake(0, 0, screenSize.width, 44))
        
        let barbuttonDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(donePressed))
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil), barbuttonDone]
        
        self.addSubview(datePicker)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @IBAction func datePickerDateChanged(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = datePicker.datePickerMode == .Time ? "hh:mm a" : kCommonDateFormat
        textField.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
    }
    func donePressed() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = datePicker.datePickerMode == .Time ? "hh:mm a" : kCommonDateFormat
        textField.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
        textField.resignFirstResponder()
    }
    
    class func addDatePickerForTextField(textField: UITextField) {
        let dateListView = DateInputView(frame: CGRectMake(0, 0, screenSize.width, 260))
        textField.inputView = dateListView
        textField.inputAccessoryView = dateListView.toolbar
        dateListView.textField = textField
        dateListView.datePicker.datePickerMode = UIDatePickerMode.Date
        
        let dateString = "1 Jan \(NSCalendar.currentCalendar().component(NSCalendarUnit.Year, fromDate: NSDate()))"
        let df = NSDateFormatter()
        df.dateFormat = "dd MM yyyy"
        let date = df.dateFromString(dateString)
        if let unwrappedDate = date {
            dateListView.datePicker.setDate(unwrappedDate, animated: false)
        }
    }
    
    class func addDatePickerForDateOfBirthTextField(textField: UITextField) {
        let dateListView = DateInputView(frame: CGRectMake(0, 0, screenSize.width, 260))
        textField.inputView = dateListView
        textField.inputAccessoryView = dateListView.toolbar
        dateListView.textField = textField
        dateListView.datePicker.datePickerMode = UIDatePickerMode.Date
        let dateString = "1 Jan 1980"
        let df = NSDateFormatter()
        df.dateFormat = "dd MM yyyy"
        let date = df.dateFromString(dateString)
        if let unwrappedDate = date {
            dateListView.datePicker.setDate(unwrappedDate, animated: false)
        }
    }
    
    
    class func addTimePickerForTextField(textField: UITextField) {
        let dateListView = DateInputView(frame: CGRectMake(0, 0, screenSize.width, 260))
        textField.inputView = dateListView
        textField.inputAccessoryView = dateListView.toolbar
        dateListView.textField = textField
        dateListView.datePicker.datePickerMode = UIDatePickerMode.Time
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        
        dateListView.datePicker.setDate(dateFormatter.dateFromString("12:00 am")!, animated: true)
    }
}
