//
//  DateLabel.swift
//  ProDental
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class DateLabel: MCLabel {

    var todayDate: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = UIColor.whiteColor()
        self.userInteractionEnabled = true
        
        font = UIFont(name: "WorkSans-Regular", size: 26)!
        textAlignment = NSTextAlignment.Center
        text = "Tap to date"
        textColor = UIColor.lightGrayColor()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(labelDateTapped))
        tapGesture.numberOfTapsRequired = 1
        addGestureRecognizer(tapGesture)
    }
    
    func labelDateTapped() {
        text = todayDate
        textColor = UIColor.blackColor()
    }
    
    var dateTapped: Bool {
        get {
            return text != "Tap to date"
        }
    }
    
    func setDate() {
        labelDateTapped()
    }
    func reset() {
        text = "Tap to date"
        textColor = UIColor.lightGrayColor()
    }
}
