//
//  BRProgressHUD.swift
//  MDRegistration
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

class BRProgressHUD: UIView {
    
    static var hudView: BRProgressHUD!

    class func sharedInstance() -> BRProgressHUD! {
        if hudView == nil {
            hudView = BRProgressHUD(frame: CGRectMake(0, 0, screenSize.width, screenSize.height))
            hudView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.2)
            
            hudView.activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
            hudView.activityView.frame = CGRectMake(0, 0, 50, 50)
            hudView.activityView.center = hudView.center
            hudView.addSubview(hudView.activityView)
        }
        return hudView
    }
    
    class func show() {
        let hudView = sharedInstance()
        hudView.activityView.startAnimating()
        UIApplication.sharedApplication().delegate?.window!!.addSubview(hudView)
        UIApplication.sharedApplication().delegate?.window!!.bringSubviewToFront(hudView)
        hudView.hidden = false
    }
    class func hide() {
        let hudView = sharedInstance()
        hudView.removeFromSuperview()
        hudView.hidden = true
        hudView.activityView.stopAnimating()
    }
    
    var activityView: UIActivityIndicatorView!
    
}
