//
//  AlertView.swift
//  ProDental
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


class PopupTextView: UIView {
    
    class func popUpView() -> PopupTextView {
        return NSBundle.mainBundle().loadNibNamed("PopupTextView", owner: nil, options: nil)!.first as! PopupTextView
    }
    
    @IBOutlet weak var textView: MCTextView!
    
    var completion:((popUpView: PopupTextView, textView: MCTextView)->Void)?
    
    func show(completion : (popUpView: PopupTextView, textView : MCTextView) -> Void) {
        self.showWithPlaceHolder("IF YES, TYPE HERE", completion: completion)
    }
    
    func showWithPlaceHolder(placeHolder : String, completion : (popUpView: PopupTextView, textView : MCTextView) -> Void) {
        textView.placeholder = placeHolder
        self.completion = completion
        
        self.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window!.addSubview(self)
        self.subviews[0].transform = CGAffineTransformMakeScale(0.1, 0.1)
        textView.placeholder = placeHolder
        textView.placeholderColor = UIColor.lightGrayColor()
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonActionOK(sender: AnyObject) {
        completion?(popUpView: self, textView: self.textView)
    }
    
    func close() {
        self.removeFromSuperview()
    }
}
