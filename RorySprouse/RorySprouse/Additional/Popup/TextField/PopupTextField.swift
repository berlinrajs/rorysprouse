//
//  PopupTextField.swift
//  FusionDental
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PopupTextField: UIView {
    
    class func popUpView() -> PopupTextField {
        return NSBundle.mainBundle().loadNibNamed("PopupTextField", owner: nil, options: nil)!.first as! PopupTextField
    }
    
    @IBOutlet weak var textField: MCTextField!
    @IBOutlet weak var labelTitle: UILabel!
    
    var completion:((PopupTextField, UITextField)->Void)?
    var count : Int!
    
    
    func show(completion : (popUpView: PopupTextField, textField : UITextField) -> Void) {
        self.showInViewController(nil, WithTitle: labelTitle.text, placeHolder: "TYPE HERE", textFormat: .Default, completion: completion)
    }
    
    func showInViewController(viewController: UIViewController?, WithTitle title: String?, placeHolder : String?, textFormat: TextFormat, completion : (popUpView: PopupTextField, textField : UITextField) -> Void) {
        textField.text = ""
        labelTitle.text = title
        textField.textFormat = textFormat
        if let _ = placeHolder {
            textField.placeholder = placeHolder
        }
        
        self.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        self.completion = completion
        if viewController != nil {
            viewController!.view.addSubview(self)
        } else {
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.window!.addSubview(self)
        }
        self.subviews[0].transform = CGAffineTransformMakeScale(0.1, 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    
    func showDatePopupInViewController(viewController: UIViewController?, WithTitle title: String?, placeHolder : String?, minDate : NSDate?, maxDate : NSDate?, completion : (popUpView: PopupTextField, textField : UITextField) -> Void) {
        textField.text = ""
        labelTitle.text = title
        textField.textFormat = .Date
        if let _ = placeHolder {
            textField.placeholder = placeHolder
        }
        
        self.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        self.completion = completion
        if viewController != nil {
            viewController!.view.addSubview(self)
        } else {
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.window!.addSubview(self)
        }
        self.subviews[0].transform = CGAffineTransformMakeScale(0.1, 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonActionOK(sender: AnyObject) {
        completion?(self, self.textField)
    }
    func close() {
        self.removeFromSuperview()
    }
}
