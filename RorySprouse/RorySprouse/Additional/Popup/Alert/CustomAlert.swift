//
//  CustomAlert.swift
//  SecureDental
//
//  Created by SRS Web Solutions on 19/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class CustomAlert: UIView {
    
    class func alertView() -> CustomAlert {
        return NSBundle.mainBundle().loadNibNamed("CustomAlert", owner: nil, options: nil)!.first as! CustomAlert
    }
    
    @IBOutlet weak var labelTitle: UILabel!
    
    var completion:(()->Void)?
    var textFormat : TextFormat!
    var count : Int!
    
    func showWithTitle(title: String?, completion:(() -> Void)) {
        self.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        labelTitle.text = title
        self.completion = completion
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window!.addSubview(self)
        self.subviews[0].transform = CGAffineTransformMakeScale(0.1, 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonAction() {
        self.removeFromSuperview()
        completion?()
    }
}
